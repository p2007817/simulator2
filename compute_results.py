import os
import json
import threading
import sys

from computings_module.results import *

USE_PROCESSES = True


def compute_heterogeneous():
    result_directory_name = sys.argv[1]
    if not os.path.isdir(result_directory_name):
        print("The script takes as argument the name of directory that contains the heterogeneous simulation results.")
        return -1

    full_result_dictionary = dict()
    scenario_directory_table = os.listdir(result_directory_name)
    if "results.json" in scenario_directory_table:
        scenario_directory_table.remove("results.json")
    for scenario_directory in scenario_directory_table:
        full_result_dictionary[scenario_directory] = dict()
        strategy_simulation_table = os.listdir(os.path.join(result_directory_name, scenario_directory))
        for strategy_simulation in strategy_simulation_table:
            strategy_simulation_result = Results(os.path.join(result_directory_name, scenario_directory), strategy_simulation)
            strategy_simulation_result.compute_results()
            full_result_dictionary[scenario_directory][strategy_simulation.split(".")[0]] = strategy_simulation_result.get_dictionary()
    f = open(os.path.join(result_directory_name, "results.json"), "w")
    f.write(json.dumps(full_result_dictionary, indent=4))
    f.close()



            
def compute_toy_examples():
    directory_name = sys.argv[1]
    if not os.path.isdir(directory_name):
        print("ERROR: The program takes as an argument the name of the diectory which contains the simulation results.")
        return -1
    if USE_PROCESSES:
        compute_with_threads(directory_name)
    else:
        compute_without_threads(directory_name)


        
def compute_with_threads(directory):
    contentions = os.listdir(directory)
    threads = []
    for contention in contentions:
        for part_dir_name in os.listdir(os.path.join(directory, contention)):
            dir_name = os.path.join(directory, contention, part_dir_name)
            if part_dir_name.startswith("FFF") or part_dir_name.startswith("TFF") or part_dir_name.startswith("FTF") or \
               part_dir_name.startswith("TTF") or part_dir_name.startswith("FFT") or part_dir_name.startswith("TFT"):
                thread = threading.Thread(target=compute_in_folder, args=(dir_name,))
                threads.append(thread)
                thread.start()
    for thread in threads:
        thread.join()
        


def compute_without_threads(directory):
    contentions = os.listdir(directory)
    for contention in contentions:
        for part_dir_name in os.listdir(os.path.join(directory, contention)):
            dir_name = os.path.join(directory, contention, part_dir_name)
            if part_dir_name.startswith("FFF") or part_dir_name.startswith("TFF") or part_dir_name.startswith("FTF") or \
               part_dir_name.startswith("TTF") or part_dir_name.startswith("FFT") or part_dir_name.startswith("TFT"):
                compute_in_folder(dir_name)


def compute_in_folder(dir_name):
    result_dictionary = dict()
    for root, dirs, files in os.walk(dir_name):
        files.sort()
        for file_name in files:
            print("Directory: ", dir_name, "\tFile: ", file_name)
            if file_name != "results.json":
                results = Results(dir_name, file_name)
                results.compute_results()
                result_dictionary[file_name.split(".")[0]] = results.get_dictionary()
    result_file = open(os.path.join(dir_name, "results.json"), "w+")
    result_file.write(json.dumps(result_dictionary, indent=4))
    result_file.close()



if __name__ == "__main__":
    compute_heterogeneous()

