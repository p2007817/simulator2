import os
import json
import matplotlib.pyplot as plt

from common_parameters import *

def main():
    contention = "MID"
    subdir_name = "FTFcontMIDbalEQ_1"
    dir_name = os.path.join("results_simulation", contention, subdir_name, "results.json")
    dict_key = ["idle avg", "CCA avg", "Rx avg", "Tx avg", "doze avg"]

    result_file = open(dir_name, "r")
    dict_tot = json.load(result_file)
    result_file.close()

    x_load = []
    y_range_table = [[] for i in range(len(dict_key))]
    for load in dict_tot:
        dict_load = dict_tot[load]
        x_load.append(dict_load["Theoritical saturation"] * 100)
        for y_range, key in zip(y_range_table,dict_key):
            y_range.append(dict_load[key])

    for y_range, key in zip(y_range_table, dict_key):
        plt.plot(x_load, y_range, label=key, marker=".", linestyle=":")
    plt.legend()
    plt.grid()
    plt.show()


    
if __name__ == "__main__":
    main()
