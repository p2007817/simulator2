import matplotlib.pyplot as plt
import os
import json
import sys
from datetime import datetime

SAVE = True

def main():
    result_directory_name = sys.argv[1]
    if not os.path.isdir(result_directory_name):
        print("ERROR: This program takes as an argument the name of the directory that contains the simulation results.")
        return -1
    
    dir_names_contLOWbalEQ = ["FFFcontLOWbalEQ", "TFFcontLOWbalEQ", "FTFcontLOWbalEQ", "TTFcontLOWbalEQ", "FFTcontLOWbalEQ", "TFTcontLOWbalEQ"]
    dir_names_contMIDbalEQ = ["FFFcontMIDbalEQ", "TFFcontMIDbalEQ", "FTFcontMIDbalEQ", "TTFcontMIDbalEQ", "FFTcontMIDbalEQ", "TFTcontMIDbalEQ"]
    dir_names_contHIGbalEQ = ["FFFcontHIGbalEQ", "TFFcontHIGbalEQ", "FTFcontHIGbalEQ", "TTFcontHIGbalEQ", "FFTcontHIGbalEQ", "TFTcontHIGbalEQ"]


    # WHICH SCENARIO?
    cont = "HIGH"
    DL_prompt_param = "4"
    slot_param = "100"

    dir_names = []
    if cont == "LOW":
        dir_names = dir_names_contLOWbalEQ
    elif cont == "MID":
        dir_names = dir_names_contMIDbalEQ
    elif cont == "HIGH":
        dir_names = dir_names_contHIGbalEQ
    for i in range(len(dir_names)):
        if dir_names[i][0] == 'T':
            dir_names[i] += "_" + slot_param
        if dir_names[i][1] == 'T':
            dir_names[i] += "_" + slot_param
        if dir_names[i][2] == 'T':
            dir_names[i] += "_" + DL_prompt_param

            
    plot_curves = Plot_curves_scenario(result_directory_name, dir_names, cont)
    plot_curves.plot_achieved_DL_throughput()
    plot_curves.plot_achieved_UL_throughput()
    plot_curves.plot_achieved_tot_throughput()
    plot_curves.plot_saturation()
    plot_curves.plot_collision_rate()
    plot_curves.plot_ratio_generated_sent_frames()
    plot_curves.plot_ratio_generated_sent_UL_frames()
    plot_curves.plot_ratio_generated_sent_DL_frames()

    plot_curves.plot_delays()
    plot_curves.plot_UL_delays()
    plot_curves.plot_DL_delays()

    plot_curves.plot_energy()
    plot_curves.plot_CCA()

class Plot_curves_scenario:

    def __init__(self, result_directory_name, dir_names, contention):
        self.result_directory_name = result_directory_name
        self.dir_names = dir_names
        self.contention = contention
        self.results_dicts = []
        self.labels = []
        self.colors = []
        self.subtitle = ""
        self.load_results()
        self.get_labels()
        self.get_subtitle()

    def load_results(self):
        for dir_name in self.dir_names:
            result_file = open(os.path.join(self.result_directory_name, self.contention, dir_name, "results.json"), "r")
            self.results_dicts.append(json.load(result_file))
            result_file.close()

    def get_labels(self):
        for dir_name in self.dir_names:
            label = ""
            color = ""
            if dir_name[:3] == "FFF":
                label = "No mechanism used"
                color = "Black"
            elif dir_name[:3] == "TFF":
                label = "UL slot"
                color = "#0080ff" # Blue
            elif dir_name[:3] == "FFT":
                label = "DL_Prompt"
                color = "#ffbf00" # Yellow
            elif dir_name[:3] == "TFT":
                label = "DL_Prompt and UL slot"
                color = "#88cc00" # Green
            elif dir_name[:3] == "FTF":
                label = "DL slot"
                color = "#ff3300" # Red
            elif dir_name[:3] == "TTF":
                label = "DL slot and UL slot"
                color = "#b30086" # Purple
            self.labels.append(label)
            self.colors.append(color)

    def get_subtitle(self):
        dir_name = self.dir_names[0]
        if dir_name[7:10] == "LOW":
            self.subtitle += "2 STAs, "
        elif dir_name[7:10] == "MID":
            self.subtitle += "7 STAs, "
        elif dir_name[7:10] == "HIG":
            self.subtitle += "20 STAs, "

        if dir_name[13:15] == "EQ":
            self.subtitle += "Upstream and downstream balanced"
                
            

    def plot_curves(self, x_ranges, y_ranges, y_errors, title, xlabel, ylabel, file_name):
        length = len(x_ranges)
        assert(len(y_ranges) == length)
        if y_errors is not None:
            assert(len(y_errors) == length)
        assert(len(self.labels) == length)
        assert(len(self.colors) == length)
        if y_errors is not None:
            for i in range(length):
                plt.errorbar(x_ranges[i], y_ranges[i], yerr = y_errors[i],
                             label = self.labels[i], linestyle = ':', marker = '.', color = self.colors[i])
        else:
            for i in range(length):
                plt.plot(x_ranges[i], y_ranges[i], label = self.labels[i],
                         linestyle = ':', marker = '.', color = self.colors[i])
        plt.legend()
        plt.grid()
        plt.suptitle(title, fontsize = 17)
        plt.title(self.subtitle)
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        if SAVE:
            name = file_name + datetime.now().strftime("%Y%m%d-%H:%M:%S")
            plt.savefig(os.path.join("results_figure", name + ".png"))
        plt.show()

        
        
    def plot_saturation(self):
        th_saturation = [[] for i in range(len(self.dir_names))]
        tot_throughput = [[] for i in range(len(self.dir_names))]
        saturation = [[] for i in range(len(self.dir_names))]
        for i in range(len(self.dir_names)):
            for j in self.results_dicts[i]:
                th_saturation[i].append(self.results_dicts[i][j]["Theoritical saturation"] * 100)
                tot_throughput[i].append(self.results_dicts[i][j]["Tot throughput avg"] / 10**6)
                saturation[i].append(self.results_dicts[i][j]["Network busy time"] * 100)
        self.plot_curves(x_ranges = th_saturation,
                         y_ranges = saturation,
                         y_errors = None,
                         title = "Saturation",
                         xlabel = "Theoritical percentage of busy time of the medium (%)",
                         # xlabel = "Average total throughput of the STA (Mbps)",
                         ylabel = "Percentage of busy time of the medium (%)",
                         file_name = "saturation")
        
    def plot_collision_rate(self):
        collision_avg = [[] for i in range(len(self.dir_names))]
        collision_std = [[] for i in range(len(self.dir_names))]
        collision_AP = [[] for i in range(len(self.dir_names))]
        th_load = [[] for i in range(len(self.dir_names))]
        for i in range(len(self.dir_names)):
            for j in self.results_dicts[i]:
                collision_avg[i].append(self.results_dicts[i][j]["Collision rate STA avg"] * 100)
                collision_std[i].append(self.results_dicts[i][j]["Collision rate STA std"] * 100)
                collision_AP[i].append(self.results_dicts[i][j]["Collision rate AP"] * 100)
                th_load[i].append(self.results_dicts[i][j]["Theoritical saturation"] * 100)
        self.plot_curves(x_ranges = th_load,
                         y_ranges = collision_avg,
                         y_errors = collision_std,
                         title = "Collision rate of STAs",
                         xlabel = "Theoritical percentage of busy time of the medium (%)",
                         ylabel = "Percentage of STA transmissions that ended up in collision (%)",
                         file_name = "Collision_STA")
        self.plot_curves(x_ranges = th_load,
                         y_ranges = collision_AP,
                         y_errors = None,
                         title = "Collision rate of the AP",
                         xlabel = "Theoritical percentage of busy time of the medium (%)",
                         ylabel = "Collision rate of the AP (%)",
                         file_name = "Collision_AP")

    def plot_achieved_tot_throughput(self):
        th_saturation = [[] for i in range(len(self.dir_names))]
        th_throughput = [[] for i in range(len(self.dir_names))]
        tot_throughput_avg = [[] for i in range(len(self.dir_names))]
        tot_throughput_std = [[] for i in range(len(self.dir_names))]
        for i in range(len(self.dir_names)):
            for j in self.results_dicts[i]:
                nb_STAs = self.results_dicts[i][j]["Number of STAs"]
                th_saturation[i].append(self.results_dicts[i][j]["Theoritical saturation"] * 100)
                th_throughput[i].append(self.results_dicts[i][j]["Theoritical tot throughput avg"] / (10**6 * nb_STAs))
                tot_throughput_avg[i].append(self.results_dicts[i][j]["Tot throughput avg"] / 10**6)
                tot_throughput_std[i].append(self.results_dicts[i][j]["Tot throughput std"] / 10**6)
        self.plot_curves(x_ranges = th_saturation,
                         y_ranges = tot_throughput_avg,
                         y_errors = tot_throughput_std,
                         title = "Total achieved throughput per STA",
                         xlabel = "Theoritical percentage of busy time of the medium (%)",
                         ylabel = "Total throughput per STA (Mb/s)",
                         file_name = "tot_throughput")

    def plot_achieved_UL_throughput(self):
        th_saturation = [[] for i in range(len(self.dir_names))]
        th_throughput = [[] for i in range(len(self.dir_names))]
        UL_throughput_avg = [[] for i in range(len(self.dir_names))]
        UL_throughput_std = [[] for i in range(len(self.dir_names))]
        for i in range(len(self.dir_names)):
            for j in self.results_dicts[i]:
                nb_STAs = self.results_dicts[i][j]["Number of STAs"]
                th_saturation[i].append(self.results_dicts[i][j]["Theoritical saturation"] * 100)
                th_throughput[i].append(self.results_dicts[i][j]["Theoritical UL throughput avg"] / (nb_STAs * 10**6))
                UL_throughput_avg[i].append(self.results_dicts[i][j]["UL throughput avg"] / 10**6)
                UL_throughput_std[i].append(self.results_dicts[i][j]["UL throughput std"] / 10**6)
        self.plot_curves(x_ranges = th_saturation,
                         y_ranges = UL_throughput_avg,
                         y_errors = UL_throughput_std,
                         title = "Achieved uplink throughput per STA",
                         xlabel = "Theoritical percentage of busy time of the medium (%)",
                         ylabel = "Uplink throughput per STA (Mb/s)",
                         file_name = "UL_throughput")

    def plot_achieved_DL_throughput(self):
        th_saturation = [[] for i in range(len(self.dir_names))]
        th_throughput = [[] for i in range(len(self.dir_names))]
        DL_throughput_avg = [[] for i in range(len(self.dir_names))]
        DL_throughput_std = [[] for i in range(len(self.dir_names))]
        for i in range(len(self.dir_names)):
            for j in self.results_dicts[i]:
                nb_STAs = self.results_dicts[i][j]["Number of STAs"]
                th_saturation[i].append(self.results_dicts[i][j]["Theoritical saturation"] * 100)
                th_throughput[i].append(self.results_dicts[i][j]["Theoritical DL throughput avg"] / (nb_STAs * 10**6))
                DL_throughput_avg[i].append(self.results_dicts[i][j]["DL throughput avg"] / 10**6)
                DL_throughput_std[i].append(self.results_dicts[i][j]["DL throughput std"] / 10**6)
        self.plot_curves(x_ranges = th_saturation,
                         y_ranges = DL_throughput_avg,
                         y_errors = DL_throughput_std,
                         title = "Achieved downlink throughput per STA",
                         xlabel = "Theoritical percentage of busy time of the medium (%)",
                         ylabel = "Downlink throughput per STA (Mb/s)",
                         file_name = "DL_throughput")

        
    # def plot_throughput_to_energy_consumption(self):
    #     energy_consumption = [[] for i in range(len(self.dir_names))]
    #     tot_throughput = [[] for i in range(len(self.dir_names))]
    #     for i in range(len(self.dir_names)):
    #         for j in self.results_dicts[i]:
    #             energy_consumption[i].append(self.results_dicts[i][j]["Consumption avg"])
    #             tot_throughput[i].append(self.results_dicts[i][j]["Total throughput"] / 10**6)
    #     self.plot_curves_diff_x_axis(x_ranges = tot_throughput,
    #                                  y_ranges = energy_consumption,
    #                                  title = "Balance between throughput and energy consumption",
    #                                  xlabel = "Total achieved throughput (uplink and downlink) (Mb/s)",
    #                                  ylabel = "Average quantity of energy consumed by one STA (J)",
    #                                  marker = True)

        

        
    def plot_ratio_generated_sent_frames(self):
        th_saturation = [[] for i in range(len(self.dir_names))]
        ratio_frames = [[] for i in range(len(self.dir_names))]
        for i in range(len(self.dir_names)):
            for j in self.results_dicts[i]:
                th_saturation[i].append(self.results_dicts[i][j]["Theoritical saturation"] * 100)
                generated_frames_avg = self.results_dicts[i][j]["Tot generated frames avg"]
                sent_frames_avg = self.results_dicts[i][j]["Tot sent frames avg"]
                ratio_frames[i].append(100 - (100 * sent_frames_avg / generated_frames_avg))
        self.plot_curves(x_ranges = th_saturation,
                         y_ranges = ratio_frames,
                         y_errors = None,
                         title = "Loss rate due to full buffers (%)",
                         xlabel = "Theoritical saturation of the medium (%)",
                         ylabel = "Percentage of generated frames that were not sent (%)",
                         file_name = "tot_gen_sent_frames")

    def plot_ratio_generated_sent_UL_frames(self):
        th_saturation = [[] for i in range(len(self.dir_names))]
        ratio_frames = [[] for i in range(len(self.dir_names))]
        for i in range(len(self.dir_names)):
            for j in self.results_dicts[i]:
                th_saturation[i].append(self.results_dicts[i][j]["Theoritical saturation"] * 100)
                generated_frames = self.results_dicts[i][j]["UL generated frames avg"]
                sent_frames = self.results_dicts[i][j]["UL sent frames avg"]
                ratio_frames[i].append(100 - (100 * sent_frames / generated_frames))
        self.plot_curves(x_ranges = th_saturation,
                         y_ranges = ratio_frames,
                         y_errors = None,
                         title = "Loss rate due to full buffers (UL) (%)",
                         xlabel = "Theoritical saturation of the medium (%)",
                         ylabel = "Percentage of generated UL frames that were not sent (%)",
                         file_name = "UL_gen_sent_frames")

    def plot_ratio_generated_sent_DL_frames(self):
        th_saturation = [[] for i in range(len(self.dir_names))]
        ratio_frames = [[] for i in range(len(self.dir_names))]
        for i in range(len(self.dir_names)):
            for j in self.results_dicts[i]:
                th_saturation[i].append(self.results_dicts[i][j]["Theoritical saturation"] * 100)
                generated_frames = self.results_dicts[i][j]["DL generated frames avg"]
                sent_frames = self.results_dicts[i][j]["DL sent frames avg"]
                ratio_frames[i].append(100 - (100 * sent_frames / generated_frames))
        self.plot_curves(x_ranges = th_saturation,
                         y_ranges = ratio_frames,
                         y_errors = None,
                         title = "Loss rate due to full buffers (DL) (%)",
                         xlabel = "Theoritical saturation of the medium (%)",
                         ylabel = "Percentage of generated DL frames that were not sent (%)",
                         file_name = "DL_gen_sent_frames")
        
    def plot_delays(self):
        th_saturation = [[] for i in range(len(self.dir_names))]
        tot_delay = [[] for i in range(len(self.dir_names))]
        std = [[] for i in range(len(self.dir_names))]
        for i in range(len(self.dir_names)):
            for j in self.results_dicts[i]:
                th_saturation[i].append(self.results_dicts[i][j]["Theoritical saturation"] * 100)
                tot_delay[i].append(self.results_dicts[i][j]["Tot delay avg"] * 10**3)
                std[i].append(self.results_dicts[i][j]["Tot delay std"] * 10**3)
        self.plot_curves(x_ranges = th_saturation,
                         y_ranges = tot_delay,
                         y_errors = std,
                         title = "Average delay on frames (UL and DL)",
                         xlabel = "Theoritical saturation (%)",
                         ylabel = "Average transmission delay on frames (ms)",
                         file_name = "tot_delay")
        
    def plot_UL_delays(self):
        th_saturation = [[] for i in range(len(self.dir_names))]
        UL_delay = [[] for i in range(len(self.dir_names))]
        std = [[] for i in range(len(self.dir_names))]
        for i in range(len(self.dir_names)):
            for j in self.results_dicts[i]:
                th_saturation[i].append(self.results_dicts[i][j]["Theoritical saturation"] * 100)
                UL_delay[i].append(self.results_dicts[i][j]["UL delay avg"] * 10**3)
                std[i].append(self.results_dicts[i][j]["UL delay std"] * 10**3)
        self.plot_curves(x_ranges = th_saturation,
                         y_ranges = UL_delay,
                         y_errors = std,
                         title = "Average delay on UL frames",
                         xlabel = "Theoritical saturation (%)",
                         ylabel = "Average transmission delay on UL frames (ms)",
                         file_name = "UL_delay")

        
    def plot_DL_delays(self):
        th_saturation = [[] for i in range(len(self.dir_names))]
        DL_delay = [[] for i in range(len(self.dir_names))]
        std = [[] for i in range(len(self.dir_names))]
        for i in range(len(self.dir_names)):
            for j in self.results_dicts[i]:
                th_saturation[i].append(self.results_dicts[i][j]["Theoritical saturation"] * 100)
                DL_delay[i].append(self.results_dicts[i][j]["DL delay avg"] * 10**3)
                std[i].append(self.results_dicts[i][j]["DL delay std"] * 10**3)
        self.plot_curves(x_ranges = th_saturation,
                         y_ranges = DL_delay,
                         y_errors = std,
                         title = "Average delay on DL frames",
                         xlabel = "Theoritical saturation (%)",
                         ylabel = "Average transmission delay on DL frames (ms)",
                         file_name = "DL_delay")
        

    def plot_energy(self):
        th_saturation = [[] for i in range(len(self.dir_names))]
        consumption_avg = [[] for i in range(len(self.dir_names))]
        consumption_std = [[] for i in range(len(self.dir_names))]
        for i in range(len(self.dir_names)):
            for j in self.results_dicts[i]:
                th_saturation[i].append(self.results_dicts[i][j]["Theoritical saturation"] * 100)
                consumption_avg[i].append(self.results_dicts[i][j]["Consumption avg"])
                consumption_std[i].append(self.results_dicts[i][j]["Consumption std"])
        self.plot_curves(x_ranges = th_saturation,
                         y_ranges = consumption_avg,
                         y_errors = consumption_std,
                         title = "Energy consumption of the STAs",
                         xlabel = "Theoritical percentage of busy time of the medium (%)",
                         ylabel = "Average energy consumption of the STAs (W)",
                         file_name = "energy")
        
    def plot_CCA(self):
        th_saturation = [[] for i in range(len(self.dir_names))]
        CCA_avg = [[] for i in range(len(self.dir_names))]
        CCA_std = [[] for i in range(len(self.dir_names))]
        for i in range(len(self.dir_names)):
            for j in self.results_dicts[i]:
                th_saturation[i].append(self.results_dicts[i][j]["Theoritical saturation"] * 100)
                CCA_avg[i].append(self.results_dicts[i][j]["CCA avg"] * 100)
                CCA_std[i].append(self.results_dicts[i][j]["CCA std"] * 100)
        self.plot_curves(x_ranges = th_saturation,
                         y_ranges = CCA_avg,
                         y_errors = CCA_std,
                         title = "Time spent in CCA busy state",
                         xlabel = "Theoritical percentage of busy time of the medium (%)",
                         ylabel = "Average percentage of time spent in CCA busy state by STA (%)",
                         file_name = "CCA")
        

        
    
if __name__ == "__main__":
    main()
