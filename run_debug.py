import sys
import os

from simulation_module.simulation import *
from simulation_module.common_parameters import *

def main():
    # scenario_test_beacon()
    # scenario_test_UL()
    # scenario_test_DL()
    scenario_test_UL_DL()
    # scenario_test_collision_UL_UL()
    # scenario_test_collision_UL_DL()
    # scenario_test_DL_slot()
    # scenario_test_DL_prompt()
    # scenario_test_UL_slot()
    # scenario_test_UL_prompt()
    # scenario_test_DL_slot_UL_slot()
    # scenario_test_DL_slot_UL_prompt()
    # scenario_test_DL_prompt_UL_slot()



###### -- SCENARIOS TEST -- ######
    
def scenario_test_beacon():
    # Beacons must be emitted once every 0.1s, and not retransmitted if in error.
    simu = Simulation()
    simu.simulation_duration = 60
    simu.run_simulation_debug()
    
def scenario_test_UL():
    simu = Simulation()
    simu.set_nb_STAs(1)
    simu.set_DL_throughput(1, 1)
    simu.set_UL_throughput(1, 10**6)
    simu.set_link_capacity(1, 100*10**6)
    simu.simulation_duration = 10
    simu.run_simulation_debug()

def scenario_test_DL():
    simu = Simulation()
    nb_STAs = 2
    simu.set_nb_STAs(nb_STAs)
    for i in range(1, nb_STAs+1):
        simu.set_DL_throughput(i, 100 * 10**6)
        simu.set_UL_throughput(i, 1)
        simu.set_link_capacity(i, 100*10**6)
    simu.simulation_duration = 10
    simu.run_simulation_debug()

def scenario_test_UL_DL():
    simu = Simulation()
    nb_STAs = 5
    simu.set_nb_STAs(nb_STAs)
    for i in range(1, nb_STAs+1):
        simu.set_DL_throughput(i, 10 * 10**6)
        simu.set_UL_throughput(i, 10 * 10**6)
        simu.set_link_capacity(i, 100 * 10**6)
    simu.simulation_duration = 10
    simu.run_simulation()
    simu.write_report("./", "test.json")


def scenario_test_collision_UL_UL():
    simu = Simulation()
    nb_STAs = 5
    simu.set_nb_STAs(nb_STAs)
    for i in range(1, nb_STAs+1):
        simu.set_DL_throughput(i, 1)
        simu.set_UL_throughput(i, 200 * 10**6)
        simu.set_link_capacity(i, 100 * 10**6)
    simu.simulation_duration = 5
    simu.run_simulation_debug()

def scenario_test_collision_UL_DL():
    simu = Simulation()
    simu.set_nb_STAs(1)
    simu.set_DL_throughput(1, 200 * 10**6)
    simu.set_UL_throughput(1, 200 * 10**6)
    simu.set_link_capacity(1, 100 * 10**6)
    simu.simulation_duration = 5
    simu.run_simulation_debug()

def scenario_test_DL_slot():
    simu = Simulation()
    nb_STAs = 20
    simu.set_nb_STAs(nb_STAs)
    for i in range(1, nb_STAs+1):
        simu.set_DL_throughput(i, 2.5 * 10**6)
        simu.set_UL_throughput(i, 2.5 * 10**6)
        interval = 1*10**-3
        duration = interval / nb_STAs
        simu.toggle_DL_slot(i, (i-1)*duration, duration, interval)
    simu.simulation_duration = 10
    simu.run_simulation()
    
def scenario_test_DL_prompt():
    simu = Simulation()
    nb_STAs = 2
    DL_prompt_interval = 5 * 10**-3
    simu.set_nb_STAs(nb_STAs)
    for i in range(1, nb_STAs+1):
        simu.set_DL_throughput(i, 5 * 10**6)
        simu.set_UL_throughput(i, 5 * 10**6)
        simu.toggle_DL_prompt(i, DL_prompt_interval)
    simu.simulation_duration = 10
    simu.run_simulation_debug()

def scenario_test_UL_slot():
    simu = Simulation()
    nb_STAs = 2
    interval = 1 * 10**-3
    simu.set_nb_STAs(nb_STAs)
    for i in range(1, nb_STAs+1):
        simu.set_DL_throughput(i, 1 * 10**6)
        simu.set_UL_throughput(i, 50 * 10**6)
        duration = interval / nb_STAs
        start = (i-1) * duration
        simu.toggle_UL_slot(i, start, duration, interval)
    simu.simulation_duration = 10
    simu.run_simulation_debug()

def scenario_test_UL_prompt():
    simu = Simulation()
    nb_STAs = 3
    prompt_interval = 1 * 10**-3
    simu.set_nb_STAs(nb_STAs)
    for i in range(1, nb_STAs+1):
        simu.set_DL_throughput(i, 10 * 10**6)
        simu.set_UL_throughput(i, 10 * 10**6)
        simu.set_link_capacity(i, 100 * 10**6)
        simu.toggle_UL_prompt(i, prompt_interval)
    simu.simulation_duration = 10
    simu.run_simulation_debug()

def scenario_test_DL_slot_UL_slot():
    # The timelines are in the order of the toggle
    # Here the timeline of DL slot appears above the timeline of the UL slot
    simu = Simulation()
    nb_STAs = 21
    simu.set_nb_STAs(nb_STAs)
    for i in range(1, nb_STAs+1):
        simu.set_DL_throughput(i, 25 * 10**3)
        simu.set_UL_throughput(i, 225 * 10**3)
        DL_interval = 10 * 10**-3
        UL_interval = 5 * 10**-3
        DL_duration = DL_interval / nb_STAs
        UL_duration = UL_interval / nb_STAs
        simu.toggle_DL_slot(i, (i-1)*DL_duration, DL_duration, DL_interval)
        simu.toggle_UL_slot(i, (i-1)*UL_duration, UL_duration, UL_interval)
    simu.set_DL_throughput(21, 0.1 * 10**6)
    simu.set_UL_throughput(21, 0.9 * 10**6)
    simu.simulation_duration = 120
    simu.run_simulation()

def scenario_test_DL_slot_UL_prompt():
    simu = Simulation()
    nb_STAs = 3
    prompt_interval = 10 * 10**-6
    slot_interval = 10 * 10**-3
    slot_duration = slot_interval / nb_STAs
    simu.set_nb_STAs(nb_STAs)
    for i in range(1, nb_STAs+1):
        simu.set_DL_throughput(i, 10 * 10**6)
        simu.set_UL_throughput(i, 10 * 10**6)
        simu.set_link_capacity(i, 100 * 10**6)
        slot_start = (i-1) * slot_duration
        simu.toggle_DL_slot(i, slot_start, slot_duration, slot_interval)
        simu.toggle_UL_prompt(i, prompt_interval)
    simu.simulation_duration = 10
    simu.run_simulation_debug()
    
def scenario_test_DL_prompt_UL_slot():
    simu = Simulation()
    nb_STAs = 21
    slot_interval = 1 * 10**-3
    prompt_interval = 0.5
    simu.set_nb_STAs(nb_STAs)
    for i in range(1, nb_STAs+1):
        simu.set_DL_throughput(i, 25 * 10**3)
        simu.set_UL_throughput(i, 225 * 10**3)
        slot_duration = slot_interval / nb_STAs
        slot_start = (i-1) * slot_duration
        simu.toggle_UL_slot(i, slot_start, slot_duration, slot_interval)
        simu.toggle_DL_prompt(i, prompt_interval)
    simu.set_DL_throughput(21, 5 * 10**6)
    simu.set_UL_throughput(21, 5 * 10**6)
    simu.simulation_duration = 10
    simu.run_simulation_debug()

    

if __name__=="__main__":
    main()
