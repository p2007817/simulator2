import matplotlib.pyplot as plt
import os
import json
from datetime import datetime

SAVE = False

def main():
    plot_curves = Plot_curves_parameters("LOW", "EQ", "020")
    plot_curves.plot_throughput_energy()

class Plot_curves_parameters():

    def __init__(self, contention, balance, load):
        self.contention = contention
        self.balance = balance
        self.load_txt = load
        
        self.dir_names = []
        self.load_dir_names()

        self.no_mech = dict()
        self.UL_TWT = dict()
        self.DL_TWT = dict()
        self.UL_TWT_DL_TWT = dict()
        self.prompt = dict()
        self.UL_TWT_prompt = dict()

        self.load_results()


    def plot_throughput_energy(self):
        self.scatter_points()

        
    def load_dir_names(self):
        self.dir_names = ["FFF", "TFF", "FTF", "TTF", "FFT", "TFT"]
        for name in self.dir_names:
            name += "cont" + self.contention
            name += "bal" + self.balance

    def load_results(self):
        for dir_name in os.listdir(os.path.join("results_simulation", self.contention)):
            print("Directory name: ", dir_name)
            if dir_name.startswith("FFF"):
                # No mechanism used
                result_file = open(os.path.join("results_simulation", self.contention, dir_name, "results.json"), "r")
                tmp_dict = json.load(result_file)
                result_file.close()
                if "load" + self.load_txt in tmp_dict:
                    self.no_mech = tmp_dict["load" + self.load_txt]

            elif dir_name.startswith("TFF"):
                # UL TWT
                param = int(dir_name.split("_")[1])
                result_file = open(os.path.join("results_simulation", self.contention, dir_name, "results.json"), "r")
                tmp_dict = json.load(result_file)
                result_file.close()
                if "load" + self.load_txt in tmp_dict:
                    self.UL_TWT[param] = tmp_dict["load" + self.load_txt]

            elif dir_name.startswith("FTF"):
                # DL TWT
                param = int(dir_name.split("_")[1])
                result_file = open(os.path.join("results_simulation", self.contention, dir_name, "results.json"), "r")
                tmp_dict = json.load(result_file)
                result_file.close()
                if "load" + self.load_txt in tmp_dict:
                    self.DL_TWT[param] = tmp_dict["load" + self.load_txt]

            elif dir_name.startswith("TTF"):
                # UL TWT + DL TWT
                param1 = int(dir_name.split("_")[1])
                #param2 = int(dir_name.split("_")[2])
                result_file = open(os.path.join("results_simulation", self.contention, dir_name, "results.json"), "r")
                tmp_dict = json.load(result_file)
                result_file.close()
                if "load" + self.load_txt in tmp_dict:
                    #self.UL_TWT_DL_TWT[str(param1) + "," + str(param2)] = tmp_dict["load" + self.load_txt]
                    self.UL_TWT_DL_TWT[param1] = tmp_dict["load" + self.load_txt]

            elif dir_name.startswith("FFT"):
                # Prompt
                param = int(dir_name.split("_")[1])
                result_file = open(os.path.join("results_simulation", self.contention, dir_name, "results.json"), "r")
                tmp_dict = json.load(result_file)
                result_file.close()
                if "load" + self.load_txt in tmp_dict:
                    self.prompt[param] = tmp_dict["load" + self.load_txt]

            elif dir_name.startswith("TFT"):
                # UL TWT + prompt
                param1 = dir_name.split("_")[1]
                param2 = dir_name.split("_")[2]
                result_file = open(os.path.join("results_simulation", self.contention, dir_name, "results.json"), "r")
                tmp_dict = json.load(result_file)
                result_file.close()
                if "load" + self.load_txt in tmp_dict:
                    self.UL_TWT_prompt[param1 + "," + param2] = tmp_dict["load" + self.load_txt]

            tmp = dict(sorted(self.UL_TWT.items()))
            self.UL_TWT = tmp
            tmp = dict(sorted(self.DL_TWT.items()))
            self.DL_TWT = tmp
            tmp = dict(sorted(self.prompt.items()))
            self.prompt = tmp
            tmp = dict(sorted(self.UL_TWT_DL_TWT.items()))
            self.UL_TWT_DL_TWT = tmp

    def scatter_points(self):
        labels = ["Sans mécanisme", "Temps de parole", "Rendez-vous", "Sollicitation", "Temps de parole et rendez-vous", "Temps de parole et sollicitation"]
        colors = ["Black", "#0080ff", "#ff3300", "#ffbf00", "#b30086", "#88cc00"]
        x_ranges = [[] for i in range(6)]
        y_ranges = [[] for i in range(6)]

        dict_key_x = "Tot throughput avg"
        dict_key_y = "Consumption avg"

        
        tmp = self.no_mech["Tot throughput avg"] / 10**6
        x_ranges[0].append(tmp)
        y_ranges[0].append(self.no_mech[dict_key_y])

        for param in self.UL_TWT:
            x_ranges[1].append(self.UL_TWT[param][dict_key_x] / 10**6)
            y_ranges[1].append(self.UL_TWT[param][dict_key_y])

        for param in self.DL_TWT:
            x_ranges[2].append(self.DL_TWT[param][dict_key_x] / 10**6)
            y_ranges[2].append(self.DL_TWT[param][dict_key_y])

        for param in self.prompt:
            x_ranges[3].append(self.prompt[param][dict_key_x] / 10**6)
            y_ranges[3].append(self.prompt[param][dict_key_y])

        for param in self.UL_TWT_DL_TWT:
            x_ranges[4].append(self.UL_TWT_DL_TWT[param][dict_key_x] / 10**6)
            y_ranges[4].append(self.UL_TWT_DL_TWT[param][dict_key_y])

        for param in self.UL_TWT_prompt:
            x_ranges[5].append(self.UL_TWT_prompt[param][dict_key_x] / 10**6)
            y_ranges[5].append(self.UL_TWT_prompt[param][dict_key_y])

        params = [None, self.UL_TWT, self.DL_TWT, self.prompt, self.UL_TWT_DL_TWT, self.UL_TWT_prompt]
        for i in range(6):
            plt.plot(x_ranges[i], y_ranges[i], color = colors[i], marker = "x", linestyle = "None", label = labels[i])
        plt.legend(loc='upper left')
        plt.grid()

        plt.xlabel("Débit moyen par STA (Mbps)", fontsize=13)
        plt.ylabel("Consommation d'énergie moyenne des STAs (W)", fontsize=13)
        if SAVE:
            plt.savefig(os.path.join("results_figure", "cores2.png"))
        plt.show()

        



if __name__ == "__main__":
    main()
