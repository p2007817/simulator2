import os
import sys
import threading

from simulation_module.simulation import *

link_capacity = 100 * 10**6

BACKeco_nb_STAs = 15
BACKperf_nb_STAs = 2

STAeco_DL_throughput = 0.1 * 10**6
STAeco_UL_throughput = 0.9 * 10**6
STAperf_DL_throughput = 5 * 10**6
STAperf_UL_throughput = 5 * 10**6

background_saturation = [0.05, 0.20, 0.40]

DL_slot_parameter_table = [5*10**-3, 10*10**-3, 100*10**-3]
DL_prompt_parameter_table = [1, 4, 8]
UL_slot_parameter_table = [5*10**-3, 10*10**-3, 100*10**-3]
UL_prompt_parameter_table = [1, 4, 8]

nb_DL_slot_simu = len(DL_slot_parameter_table)
nb_DL_prompt_simu = len(DL_prompt_parameter_table)
nb_UL_slot_simu = len(UL_slot_parameter_table)
nb_UL_prompt_simu = len(UL_prompt_parameter_table)
nb_DL_slot_UL_slot_simu = len(DL_slot_parameter_table) * len(UL_slot_parameter_table)
nb_DL_slot_UL_prompt_simu = len(DL_slot_parameter_table) * len(UL_prompt_parameter_table)
nb_DL_prompt_UL_slot_simu = len(DL_prompt_parameter_table) * len(UL_slot_parameter_table)

result_dir = sys.argv[1]



def main():
    for saturation in background_saturation:
        print("=== SATURATION ", saturation, " ===")
        eco_eco_pid = os.fork()
        if eco_eco_pid == 0:
            # eco_eco_thread = threading.Thread(target=run_all_strategies, args=("BACKeco_STAeco", saturation))
            # eco_eco_thread.start()
            run_all_strategies("BACKeco_STAeco", saturation)
            return 0
        eco_perf_pid = os.fork()
        if eco_perf_pid == 0:
            # eco_perf_thread = threading.Thread(target=run_all_strategies, args=("BACKeco_STAperf", saturation))
            # eco_perf_thread.start()
            run_all_strategies("BACKeco_STAperf", saturation)
            return 0
        perf_eco_pid = os.fork()
        if perf_eco_pid == 0:
            run_all_strategies("BACKperf_STAeco", saturation)
            return 0
            # perf_eco_thread = threading.Thread(target=run_all_strategies, args=("BACKperf_STAeco", saturation))
            # perf_eco_thread.start()
        perf_perf_pid = os.fork()
        if perf_perf_pid == 0:
            run_all_strategies("BACKperf_STAperf", saturation)
            return 0
            # perf_perf_thread = threading.Thread(target=run_all_strategies, args=("BACKperf_STAperf", saturation))
            # perf_perf_thread.start()
        # perf_perf_thread.join()
        # perf_eco_thread.join()
        # eco_perf_thread.join()
        # eco_eco_thread.join()
        os.waitpid(perf_perf_pid, 0)
        os.waitpid(perf_eco_pid, 0)
        os.waitpid(eco_perf_pid, 0)
        os.waitpid(eco_eco_pid, 0)



        
### --- PIPELINE: SCENARIO PARAMETRIZATION ---

def parametrize_BACKeco_STAeco(simulation, saturation):
    nb_STAs = BACKeco_nb_STAs + 1
    simulation.set_nb_STAs(nb_STAs)
    
    simulation.set_link_capacity(1, link_capacity)
    simulation.set_DL_throughput(1, STAeco_DL_throughput)
    simulation.set_UL_throughput(1, STAeco_UL_throughput)
    
    DL_saturation_per_STA = saturation / (10 * nb_STAs)
    UL_saturation_per_STA = 9 * saturation / (10 * nb_STAs)
    DL_throughput_per_STA = DL_saturation_per_STA * link_capacity
    UL_throughput_per_STA = UL_saturation_per_STA * link_capacity
    for i in range(2, nb_STAs+1):
        simulation.set_link_capacity(i, link_capacity)
        simulation.set_DL_throughput(i, DL_throughput_per_STA)
        simulation.set_UL_throughput(i, UL_throughput_per_STA)

def parametrize_BACKeco_STAperf(simulation, saturation):
    nb_STAs = BACKeco_nb_STAs + 1
    simulation.set_nb_STAs(nb_STAs)
    
    simulation.set_link_capacity(1, link_capacity)
    simulation.set_DL_throughput(1, STAperf_DL_throughput)
    simulation.set_UL_throughput(1, STAperf_UL_throughput)

    DL_saturation_per_STA = saturation / (10 * nb_STAs)
    UL_saturation_per_STA = 9 * saturation / (10 * nb_STAs)
    DL_throughput_per_STA = DL_saturation_per_STA * link_capacity
    UL_throughput_per_STA = UL_saturation_per_STA * link_capacity
    for i in range(2, nb_STAs+1):
        simulation.set_link_capacity(i, link_capacity)
        simulation.set_DL_throughput(i, DL_throughput_per_STA)
        simulation.set_UL_throughput(i, UL_throughput_per_STA)

def parametrize_BACKperf_STAeco(simulation, saturation):
    nb_STAs = BACKperf_nb_STAs + 1
    simulation.set_nb_STAs(nb_STAs)

    simulation.set_link_capacity(1, link_capacity)
    simulation.set_DL_throughput(1, STAeco_DL_throughput)
    simulation.set_UL_throughput(1, STAeco_UL_throughput)

    DL_saturation_per_STA = saturation / (2 * nb_STAs)
    UL_saturation_per_STA = saturation / (2 * nb_STAs)
    DL_throughput_per_STA = DL_saturation_per_STA * link_capacity
    UL_throughput_per_STA = UL_saturation_per_STA * link_capacity
    for i in range(2, nb_STAs+1):
        simulation.set_link_capacity(i, link_capacity)
        simulation.set_DL_throughput(i, DL_throughput_per_STA)
        simulation.set_UL_throughput(i, UL_throughput_per_STA)

def parametrize_BACKperf_STAperf(simulation, saturation):
    nb_STAs = BACKperf_nb_STAs + 1
    simulation.set_nb_STAs(nb_STAs)

    simulation.set_link_capacity(1, link_capacity)
    simulation.set_DL_throughput(1, STAperf_DL_throughput)
    simulation.set_UL_throughput(1, STAperf_UL_throughput)

    DL_saturation_per_STA = saturation / (2 * nb_STAs)
    UL_saturation_per_STA = saturation / (2 * nb_STAs)
    DL_throughput_per_STA = DL_saturation_per_STA * link_capacity
    UL_throughput_per_STA = UL_saturation_per_STA * link_capacity
    for i in range(2, nb_STAs+1):
        simulation.set_link_capacity(i, link_capacity)
        simulation.set_DL_throughput(i, DL_throughput_per_STA)
        simulation.set_UL_throughput(i, UL_throughput_per_STA)

        
### --- PIPELINE: STRATEGY PARAMETRIZATION ---

def parametrize_DL_slot(simulation, parameter):
    nb_STAs = simulation.get_nb_STAs()
    assert(nb_STAs > 0)
    slot_duration = parameter / nb_STAs
    slot_interval = parameter
    for i in range(1, nb_STAs):
        slot_start = (i-1) * slot_duration
        simulation.toggle_DL_slot(i, slot_start, slot_duration, slot_interval)

def parametrize_DL_prompt(simulation, parameter):
    nb_STAs = simulation.get_nb_STAs()
    for i in range(1, nb_STAs):
        DL_frame_frequency = simulation.get_DL_frame_frequency(i)
        prompt_parameter = parameter / DL_frame_frequency
        simulation.toggle_DL_prompt(i, prompt_parameter)
        

def parametrize_UL_slot(simulation, parameter):
    nb_STAs = simulation.get_nb_STAs()
    assert(nb_STAs > 0)
    slot_duration = parameter / nb_STAs
    slot_interval = parameter
    for i in range(1, nb_STAs):
        slot_start = (i-1) * slot_duration
        simulation.toggle_UL_slot(i, slot_start, slot_duration, slot_interval)

def parametrize_UL_prompt(simulation, parameter):
    nb_STAs = simulation.get_nb_STAs()
    for i in range(1, nb_STAs):
        UL_frame_frequency = simulation.get_UL_frame_frequency(i)
        prompt_parameter = parameter / UL_frame_frequency
        simulation.toggle_UL_prompt(i, prompt_parameter)

def parametrize_DL_slot_UL_slot(simulation, DL_parameter, UL_parameter):
    nb_STAs = simulation.get_nb_STAs()
    assert(nb_STAs > 0)
    DL_slot_duration = DL_parameter / nb_STAs
    UL_slot_duration = UL_parameter / nb_STAs
    DL_slot_interval = DL_parameter
    UL_slot_interval = UL_parameter
    for i in range(1, nb_STAs):
        DL_slot_start = (i-1) * DL_slot_duration
        UL_slot_start = (i-1) * UL_slot_duration
        simulation.toggle_DL_slot(i, DL_slot_start, DL_slot_duration, DL_slot_interval)
        simulation.toggle_UL_slot(i, UL_slot_start, UL_slot_duration, UL_slot_interval)

def parametrize_DL_slot_UL_prompt(simulation, DL_parameter, UL_parameter):
    nb_STAs = simulation.get_nb_STAs()
    assert(nb_STAs > 0)
    DL_slot_duration = DL_parameter / nb_STAs
    DL_slot_interval = DL_parameter
    for i in range(1, nb_STAs):
        DL_slot_start = (i-1) * DL_slot_duration
        simulation.toggle_DL_slot(i, DL_slot_start, DL_slot_duration, DL_slot_interval)
        UL_frame_frequency = simulation.get_UL_frame_frequency(i)
        prompt_parameter = parameter / UL_frame_frequency
        simulation.toggle_UL_prompt(i, prompt_parameter)

def parametrize_DL_prompt_UL_slot(simulation, DL_parameter, UL_parameter):
    nb_STAs = simulation.get_nb_STAs()
    assert(nb_STAs > 0)
    UL_slot_duration = UL_parameter / nb_STAs
    UL_slot_interval = UL_parameter
    for i in range(1, nb_STAs):
        UL_slot_start = (i-1) * UL_slot_duration
        simulation.toggle_UL_slot(i, UL_slot_start, UL_slot_duration, UL_slot_interval)
        DL_frame_frequency = simulation.get_DL_frame_frequency(i)
        prompt_parameter = parameter / DL_frame_frequency
        simulation.toggle_DL_prompt(i, prompt_parameter)



### --- RUN SIMULATION FOR STRATEGIES (with a given scenario) ---

def run_no_strategy(simulation, result_repertory_name):
    simulation.run_simulation()
    simulation.write_report(result_repertory_name, "no_strategy.json")


def run_DL_slot_strategy(simulation_table, result_repertory_name):
    assert(len(DL_slot_parameter_table) == len(simulation_table))
    simulation_index = 0
    for DL_slot_parameter in DL_slot_parameter_table:
        simulation = simulation_table[simulation_index]
        parametrize_DL_slot(simulation, DL_slot_parameter)
        simulation.run_simulation()
        parameter_str = str(int(DL_slot_parameter * 10**3))
        simulation.write_report(result_repertory_name, "DL_slot" + parameter_str + ".json")
        simulation_index += 1

def run_DL_prompt_strategy(simulation_table, result_repertory_name):
    assert(len(DL_prompt_parameter_table) == len(simulation_table))
    simulation_index = 0
    for DL_prompt_parameter in DL_prompt_parameter_table:
        simulation = simulation_table[simulation_index]
        parametrize_DL_prompt(simulation, DL_prompt_parameter)
        simulation.run_simulation()
        parameter_str = str(int(DL_prompt_parameter))
        simulation.write_report(result_repertory_name, "DL_prompt" + parameter_str + ".json")
        simulation_index += 1

def run_UL_slot_strategy(simulation_table, result_repertory_name):
    assert(len(UL_slot_parameter_table) == len(simulation_table))
    simulation_index = 0
    for UL_slot_parameter in UL_slot_parameter_table:
        simulation = simulation_table[simulation_index]
        parametrize_UL_slot(simulation, UL_slot_parameter)
        simulation.run_simulation()
        parameter_str = str(int(UL_slot_parameter * 10**3))
        simulation.write_report(result_repertory_name, "UL_slot_" + parameter_str + ".json")
        simulation_index += 1
    
def run_UL_prompt_strategy(simulation_table, result_repertory_name):
    assert(len(UL_prompt_parameter_table) == len(simulation_table))
    simulation_index = 0
    for UL_prompt_parameter in UL_prompt_parameter_table:
        simulation = simulation_table[simulation_index]
        parametrize_UL_prompt(simulation, UL_prompt_parameter)
        simulation.run_simulation()
        parameter_str = str(int(UL_prompt_parameter))
        simulation.write_report(result_repertory_name, "UL_prompt" + parameter_str + ".json")
        simulation_index += 1

def run_DL_slot_UL_slot_strategy(simulation_table, result_repertory_name):
    assert(len(DL_slot_parameter_table) * len(UL_slot_parameter_table) == len(simulation_table))
    simulation_index = 0
    for DL_slot_parameter in DL_slot_parameter_table:
        for UL_slot_parameter in UL_slot_parameter_table:
            simulation = simulation_table[simulation_index]
            parametrize_DL_slot(simulation, DL_slot_parameter)
            parametrize_UL_slot(simulation, UL_slot_parameter)
            simulation.run_simulation()
            parameter_str = str(int(DL_slot_parameter * 10**3))
            parameter_str += str(int(UL_slot_parameter * 10**3))
            simulation.write_report(result_repertory_name, "DL_slot_UL_slot" + parameter_str + ".json")
            simulation_index += 1

def run_DL_slot_UL_prompt_strategy(simulation_table, result_repertory_name):
    assert(len(DL_slot_parameter_table) * len(UL_prompt_parameter_table) == len(simulation_table))
    simulation_index = 0
    for DL_slot_parameter in DL_slot_parameter_table:
        for UL_prompt_parameter in UL_prompt_parameter_table:
            simulation = simulation_table[simulation_index]
            parametrize_DL_slot(simulation, DL_slot_parameter)
            parametrize_UL_prompt(simulation, UL_prompt_parameter)
            simulation.run_simulation()
            parameter_str = str(int(DL_slot_parameter * 10**3))
            parameter_str += str(int(UL_prompt_parameter))
            simulation.write_report(result_repertory_name, "DL_slot_UL_prompt" + parameter_str + ".json")
            simulation_index += 1

def run_DL_prompt_UL_slot_strategy(simulation_table, result_repertory_name):
    assert(len(DL_prompt_parameter_table) * len(UL_slot_parameter_table) == len(simulation_table))
    simulation_index = 0
    for DL_prompt_parameter in DL_prompt_parameter_table:
        for UL_slot_parameter in UL_slot_parameter_table:
            simulation = simulation_table[simulation_index]
            parametrize_DL_prompt(simulation, DL_prompt_parameter)
            parametrize_UL_slot(simulation, UL_slot_parameter)
            simulation.run_simulation() 
            parameter_str = str(int(DL_prompt_parameter))
            parameter_str += str(int(UL_slot_parameter * 10**3))
            simulation.write_report(result_repertory_name, "DL_prompt_UL_slot" + parameter_str + ".json")
            simulation_index += 1


### --- RUN DIFFERENT SCENARIO ---

def run_all_strategies(scenario, saturation):
    assert(scenario in ["BACKeco_STAeco", "BACKeco_STAperf", "BACKperf_STAeco", "BACKperf_STAperf"])

    repertory_name = scenario + "_" + str(int(saturation * 100))
    if not os.path.isdir(os.path.join(result_dir, repertory_name)):
        os.mkdir(os.path.join(result_dir, repertory_name))
    no_strategy_simu = Simulation()
    DL_slot_simu_table = [Simulation() for i in range(nb_DL_slot_simu)]
    DL_prompt_simu_table = [Simulation() for i in range(nb_DL_prompt_simu)]
    UL_slot_simu_table = [Simulation() for i in range(nb_UL_slot_simu)]
    UL_prompt_simu_table = [Simulation() for i in range(nb_UL_prompt_simu)]
    DL_slot_UL_slot_simu_table = [Simulation() for i in range(nb_DL_slot_UL_slot_simu)]
    DL_slot_UL_prompt_simu_table = [Simulation() for i in range(nb_DL_slot_UL_prompt_simu)]
    DL_prompt_UL_slot_simu_table = [Simulation() for i in range(nb_DL_prompt_UL_slot_simu)]

    all_simu = [no_strategy_simu] + DL_slot_simu_table + DL_prompt_simu_table
    all_simu += UL_slot_simu_table + UL_prompt_simu_table
    all_simu += DL_slot_UL_slot_simu_table + DL_slot_UL_prompt_simu_table + DL_prompt_UL_slot_simu_table

    for simulation in all_simu:
        if scenario == "BACKeco_STAeco":
            parametrize_BACKeco_STAeco(simulation, saturation)
        elif scenario == "BACKeco_STAperf":
            parametrize_BACKeco_STAperf(simulation, saturation)
        elif scenario == "BACKperf_STAeco":
            parametrize_BACKperf_STAeco(simulation, saturation)
        elif scenario == "BACKperf_STAperf":
            parametrize_BACKperf_STAperf(simulation, saturation)

    no_strategy_pid = os.fork()
    if no_strategy_pid == 0:
        run_no_strategy(no_strategy_simu, os.path.join(result_dir, repertory_name))
        return 0
    DL_slot_pid = os.fork()
    if DL_slot_pid == 0:
        run_DL_slot_strategy(DL_slot_simu_table, os.path.join(result_dir, repertory_name))
        return 0
    DL_prompt_pid = os.fork()
    if DL_prompt_pid == 0:
        run_DL_prompt_strategy(DL_prompt_simu_table, os.path.join(result_dir, repertory_name))
        return 0
    UL_slot_pid = os.fork()
    if UL_slot_pid == 0:
        run_UL_slot_strategy(UL_slot_simu_table, os.path.join(result_dir, repertory_name))
        return 0
    UL_prompt_pid = os.fork()
    if UL_prompt_pid == 0:
        run_UL_prompt_strategy(UL_prompt_simu_table, os.path.join(result_dir, repertory_name))
        return 0
    DL_slot_UL_slot_pid = os.fork()
    if DL_slot_UL_slot_pid == 0:
        run_DL_slot_UL_slot_strategy(DL_slot_UL_slot_simu_table, os.path.join(result_dir, repertory_name))
        return 0
    DL_slot_UL_prompt_pid = os.fork()
    if DL_slot_UL_prompt_pid == 0:
        run_DL_slot_UL_prompt_strategy(DL_slot_UL_prompt_simu_table, os.path.join(result_dir, repertory_name))
        return 0
    DL_prompt_UL_slot_pid = os.fork()
    if DL_prompt_UL_slot_pid == 0:
        run_DL_prompt_UL_slot_strategy(DL_prompt_UL_slot_simu_table, os.path.join(result_dir, repertory_name))
        return 0
    
    os.waitpid(no_strategy_pid, 0)
    print("\t",  scenario, "No strategy --- OK")
    os.waitpid(DL_slot_pid, 0)
    print("\t", scenario, "--- DL slot --- OK")
    os.waitpid(DL_prompt_pid, 0)
    print("\t", scenario, "--- DL prompt --- OK")
    os.waitpid(UL_slot_pid, 0)
    print("\t", scenario, "--- UL slot --- OK")
    os.waitpid(UL_prompt_pid, 0)
    print("\t", scenario, "--- UL prompt --- OK")
    os.waitpid(DL_slot_UL_slot_pid, 0)
    print("\t", scenario, "--- DL slot UL slot --- OK")
    os.waitpid(DL_slot_UL_prompt_pid, 0)
    print("\t", scenario, "--- DL slot UL prompt --- OK")
    os.waitpid(DL_prompt_UL_slot_pid, 0)
    print("\t", scenario, "--- DL prompt UL slot --- OK")            



if __name__=="__main__":
    main()
