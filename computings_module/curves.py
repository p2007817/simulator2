from datetime import datetime
import matplotlib.pyplot as plt
import os

from common_parameters import *

class Curves:

    def __init__(self):
        self.x_ranges = None
        self.x_errors = None
        self.y_ranges = None
        self.y_errors = None
        self.labels = None
        self.colors = None
        self.title = None
        self.subtitle = None
        self.xlabel = None
        self.ylabel = None
        self.x_lower_bound = None
        self.y_lower_bound = None
        self.x_upper_bound = None
        self.y_upper_bound = None
        self.x_min = None
        self.x_max = None
        self.y_min = None
        self.y_max = None
        self.filename = None
        self.markers = None

    def show_figure(self):
        nb_curves = len(self.x_ranges)
        assert(len(self.y_ranges) == nb_curves)
        if self.x_errors is not None:
            assert(len(self.x_errors) == nb_curves)
        if self.y_errors is not None:
            assert(len(self.y_errors) == nb_curves)
        assert(len(self.labels) == nb_curves)
        assert(len(self.colors) == nb_curves)
        assert(len(self.markers) == nb_curves)

        for i in range(nb_curves):
            plt.plot(self.x_ranges[i], self.y_ranges[i], linestyle=":",
                     marker=self.markers[i], label=self.labels[i], color=self.colors[i])

        if self.x_errors is not None:
            for i in range(nb_curves):
                plt.errorbar(x_ranges[i], y_ranges[i], xerr=self.x_errors[i], linestyle="", marker="", color=self.colors[i])

        if self.y_errors is not None:
            for i in range(nb_curves):
                plt.errorbar(x_ranges[i], y_ranges[i], yerr=self.y_errors[i], linestyle="", marker="", color=self.colors[i])

        if self.x_lower_bound is not None and self.y_lower_bound is not None and \
           self.x_upper_bound is not None and self.y_upper_bound is not None:
            plt.plot(self.x_lower_bound, self.y_lower_bound, linestyle="--", linewidth=1, color="grey", label="Lower/upper bound")
            plt.plot(self.x_upper_bound, self.y_upper_bound, linestyle="--", linewidth=1, color="grey")
        elif self.x_lower_bound is not None and self.y_lower_bound is not None:
            plt.plot(self.x_lower_bound, self.y_lower_bound, linestyle="--", linewidth=1, color="grey", label="Lower bound")
        elif self.x_upper_bound is not None and self.y_upper_bound is not None:
            plt.plot(self.x_upper_bound, self.y_upper_bound, linestyle="--", linewidth=1, color="grey", label="Upper bound")

        if len(self.labels) <= 10:
            plt.legend()
        else:
            plt.legend(fontsize=8, loc='center left', bbox_to_anchor=(1, 0.5))
            plt.subplots_adjust(right=0.7)
            # plt.set_position()
            # plt.tight_layout()


        plt.grid()
        plt.suptitle(self.title, fontsize=17)
        plt.title(self.subtitle, fontsize=11)
        plt.xlabel(self.xlabel, fontsize=12)
        plt.ylabel(self.ylabel, fontsize=12)
        if self.x_min is not None and self.x_max is not None:
            assert(self.x_min < self.x_max)
            plt.xlim(xmin=self.x_min, xmax=self.x_max)
        if self.y_min is not None and self.y_max is not None:
            assert(self.y_min < self.y_max)
            plt.ylim(ymin=self.y_min, ymax=self.y_max)

        if SAVE:
            name = self.filename + datetime.now().strftime("%Y%m%d-%H:%M:%S") + ".png"
            plt.savefig(name)
        if SHOW:
            plt.show()
        else:
            plt.clf()
