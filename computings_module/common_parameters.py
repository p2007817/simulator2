SHOW = False
SAVE = True


# TODO: Proper calibration
IDLE_CONSUMPTION = 1.4 #Watt
CCA_CONSUMPTION = 1.4 #Watt
RX_CONSUMPTION = 1.6 #Watt
TX_CONSUMPTION = 1.9 #Watt
DOZE_CONSUMPTION = 0.3 #Watt


# Colors/markers used in curves
### UL slot
#UL_slot_markers = {"1":"1", "5":"2", "10":"3", "50":"4", "100":"+"}
UL_slot_markers = {"1":"+", "5":"v", "10":"x", "50":"*", "100":"D"}

### DL slot
# Yellow
DL_slot_colors = {"1":"#4d0123", "5":"#77002b", "10":"#a4011e", "50":"#ca0f0f", "100":"#f14227"}

### DL_Prompt
# Magenta
DL_prompt_colors = {"1":"#5c4800", "4":"#9e8303", "8":"#ecd32b"}

### DL slot + UL slot: colors are based on DL slot parameters
# Green
UL_DL_slot_colors = {"1":"#1b005a", "5":"#3e019d", "10":"#5f05cb", "50":"#8f15ed", "100":"#cb40f5"}

### DL_prompt + UL slot
# PURPLE
# Colors are based on DL_prompt parameters
UL_slot_DL_prompt_colors = {"1":"#b6e600", "4":"#0a8a00", "8":"#00511a"}
