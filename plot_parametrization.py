import os
import json
import sys
import matplotlib.pyplot as plt
from sortedcontainers import SortedSet

from common_parameters import *
from curves import *

def main():
    result_directory_name = sys.argv[1]
    mechanism = sys.argv[2]
    contention = sys.argv[3]
    balancing = sys.argv[4]

    if not os.path.isdir(result_directory_name):
        print("ERROR: The 1st argument is the directory name where the simulation results are.")
        return -1
    if not mechanism in ["UL_slot", "DL_slot", "DL_prompt", "UL_DL_slot", "UL_slot_DL_prompt"]:
        print("ERROR: The second argument must be the considered mechanism.")
        print("Possible values: UL_slot, DL_slot, DL_prompt, UL_DL_slot, UL_slot_DL_prompt")
        return -1
    if not contention in ["LOW", "MID", "HIGH", "MAX"]:
        print("ERROR: The 3rd argument must be the contention")
        print("Possible values: LOW, MID, HIGH, MAX")
        return -1
    if not balancing in ["EQ", "UL", "DL"]:
        print("ERROR: The 4th argument must be the balancing between the upstream and the downstream traffic.")
        print("Possible values: EQ, DL, UL")
        return -1

    curves = Plot_parametrization(result_directory_name, mechanism, contention, balancing)
    curves.plot_energy_efficiency()
    curves.plot_throughput()
    curves.plot_delay()
    curves.plot_doze()

class Plot_parametrization:

    def __init__(self, result_directory_name, mechanism, contention, balancing):

        self.result_directory_name = result_directory_name
        self.mechanism = mechanism
        self.contention = contention
        self.balancing = balancing

        self.result_dict_table = []
        self.result_dict_no_mech = dict()

        self.params = []

        self.labels = []
        self.label_no_mech = "No mechanism"
        self.colors = []
        self.color_no_mech = "#000000"
        self.markers = []
        self.marker_no_mech = "."
        self.subtitle = ""
        
        self.load_results()
        self.get_labels_and_colors()

        
    def load_results(self):
        dir_name_prefix = ""
        if self.mechanism == "UL_slot":
            dir_name_prefix = "TFF"
        elif self.mechanism == "DL_slot":
            dir_name_prefix = "FTF"
        elif self.mechanism == "DL_prompt":
            dir_name_prefix = "FFT"
        elif self.mechanism == "UL_DL_slot":
            dir_name_prefix = "TTF"
        elif self.mechanism == "UL_slot_DL_prompt":
            dir_name_prefix = "TFT"
        dir_name_prefix += "cont" + self.contention[:3] + "bal" + self.balancing

        results_no_mech = dict()
        for dir_name in os.listdir(os.path.join(self.result_directory_name, self.contention)):
            if dir_name.startswith(dir_name_prefix):
                result_file = open(os.path.join(self.result_directory_name, self.contention, dir_name, "results.json"), "r")
                dict_tmp = json.load(result_file)
                result_file.close()
                param_txt = dir_name.split("_")[1]
                param_tmp = int(param_txt)
                if self.mechanism == "UL_slot_DL_prompt" or self.mechanism == "UL_DL_slot":
                    param_txt_1 = dir_name.split("_")[1]
                    param_txt_2 = dir_name.split("_")[2]
                    param_tmp = (int(param_txt_1), int(param_txt_2))
                insert_index = 0
                while insert_index < len(self.params) and param_tmp < self.params[insert_index]:
                    insert_index += 1
                self.params.insert(insert_index, param_tmp)
                self.result_dict_table.insert(insert_index, dict_tmp)
            elif dir_name.startswith("FFF"):
                result_file = open(os.path.join(self.result_directory_name, self.contention, dir_name, "results.json"), "r")
                self.result_dict_no_mech = json.load(result_file)
                result_file.close()
        


    def get_labels_and_colors(self):
        slot_suffix_label = " ms"
        DL_prompt_suffix_label = " TU"

        for param in self.params:
            if self.mechanism == "UL_slot":
                self.markers.append(UL_slot_markers[str(param)])
                self.colors.append("blue")
                self.labels.append(str(param) + slot_suffix_label)
                
            elif self.mechanism == "DL_slot":
                self.markers.append(".")
                self.colors.append(DL_slot_colors[str(param)])
                self.labels.append(str(param) + slot_suffix_label)
                
            elif self.mechanism == "DL_prompt":
                self.markers.append(".")
                self.colors.append(DL_prompt_colors[str(param)])
                self.labels.append(str(param) + DL_prompt_suffix_label)
                
            elif self.mechanism == "UL_DL_slot":
                self.markers.append(UL_slot_markers[str(param[0])])
                self.colors.append(UL_DL_slot_colors[str(param[1])])
                self.labels.append("UL: " + str(param[0]) + slot_suffix_label + ",DL: " + str(param[1]) + slot_suffix_label)
                
            elif self.mechanism == "UL_slot_DL_prompt":
                self.markers.append(UL_slot_markers[str(param[0])])
                self.colors.append(UL_slot_DL_prompt_colors[str(param[1])])
                self.labels.append(str(param[0]) + slot_suffix_label + str(param[1]) + DL_prompt_suffix_label)

        if self.contention == "LOW":
            self.subtitle = "2 STAs"
        elif self.contention == "MID":
            self.subtitle = "7 STAs"
        elif self.contention == "HIGH":
            self.subtitle = "20 STAs"

        if self.balancing == "EQ":
            self.subtitle += " ; UL and DL traffic equal"

    def plot_energy_efficiency(self):
        mech_x_range_table = []
        mech_y_range_table = []
        no_mech_x_range = []
        no_mech_y_range = []
        upper_bound_x_range = []
        upper_bound_y_range = []

        for result_dict in self.result_dict_table:
            mech_x_range = []
            mech_y_range = []
            for load in result_dict:
                throughput = result_dict[load]["Tot throughput avg"] / (10**6)
                energy_consumption = result_dict[load]["Consumption avg"]
                mech_x_range.append(result_dict[load]["Theoritical saturation"] * 100)
                mech_y_range.append(throughput / energy_consumption)
            mech_x_range_table.append(mech_x_range)
            mech_y_range_table.append(mech_y_range)

        for load in self.result_dict_no_mech:
            # No mechanism
            throughput = self.result_dict_no_mech[load]["Tot throughput avg"] / (10**6)
            energy_consumption = self.result_dict_no_mech[load]["Consumption avg"]
            no_mech_x_range.append(self.result_dict_no_mech[load]["Theoritical saturation"] * 100)
            no_mech_y_range.append(throughput / energy_consumption)

            # Upper bound
            th_DL_throughput = self.result_dict_no_mech[load]["Theoritical DL throughput avg"] / (10**6)
            th_UL_throughput = self.result_dict_no_mech[load]["Theoritical UL throughput avg"] / (10**6)
            th_throughput = min(100, th_DL_throughput + th_UL_throughput)
            th_STA_saturation = min(1, (th_DL_throughput + th_UL_throughput) / 100)
            th_energy_consumption = RX_CONSUMPTION * th_DL_throughput / 100
            th_energy_consumption += TX_CONSUMPTION * th_UL_throughput / 100
            th_energy_consumption += DOZE_CONSUMPTION * (1 - th_STA_saturation)
            upper_bound_x_range.append(self.result_dict_no_mech[load]["Theoritical saturation"] * 100)
            upper_bound_y_range.append(th_throughput / th_energy_consumption)

        curve = Curves()
        curve.x_ranges = mech_x_range_table + [no_mech_x_range]
        curve.y_ranges = mech_y_range_table + [no_mech_y_range]
        curve.labels = self.labels + [self.label_no_mech]
        curve.colors = self.colors + [self.color_no_mech]
        curve.markers = self.markers + [self.marker_no_mech]
        curve.title = "Energy efficiency of the STAs"
        curve.subtitle = self.subtitle
        curve.xlabel = "Theoritical percentage of busy time of the medium (%)"
        curve.ylabel = "Energy efficiency of the STAs (Mb/J)"
        curve.x_upper_bound = upper_bound_x_range
        curve.y_upper_bound = upper_bound_y_range
        curve.filename = os.path.join("results_figure", "parametrization", self.contention, self.mechanism, "energy_efficiency")
        curve.show_figure()


    def plot_throughput(self):
        mech_x_range_table = []
        mech_y_range_table_UL = []
        mech_y_range_table_DL = []
        no_mech_x_range = []
        no_mech_y_range_UL = []
        no_mech_y_range_DL = []
        upper_bound_x_range = []
        upper_bound_y_range_UL = []
        upper_bound_y_range_DL = []

        for result_dict in self.result_dict_table:
            mech_x_range = []
            mech_y_range_UL = []
            mech_y_range_DL = []
            for load in result_dict:
                mech_x_range.append(result_dict[load]["Theoritical saturation"] * 100)
                mech_y_range_UL.append(result_dict[load]["UL throughput avg"] / 10**6)
                mech_y_range_DL.append(result_dict[load]["DL throughput avg"] / 10**6)
            mech_x_range_table.append(mech_x_range)
            mech_y_range_table_UL.append(mech_y_range_UL)
            mech_y_range_table_DL.append(mech_y_range_DL)

        for load in self.result_dict_no_mech:
            # No mechanism
            no_mech_x_range.append(self.result_dict_no_mech[load]["Theoritical saturation"] * 100)
            no_mech_y_range_UL.append(self.result_dict_no_mech[load]["UL throughput avg"] / 10**6)
            no_mech_y_range_DL.append(self.result_dict_no_mech[load]["DL throughput avg"] / 10**6)
            # Upper bound
            upper_bound_x_range.append(self.result_dict_no_mech[load]["Theoritical saturation"] * 100)
            upper_bound_y_range_UL.append(self.result_dict_no_mech[load]["Theoritical UL throughput avg"] / 10**6)
            upper_bound_y_range_DL.append(self.result_dict_no_mech[load]["Theoritical DL throughput avg"] / 10**6)

        # Curve for UL
        curve_UL = Curves()
        curve_UL.x_ranges = mech_x_range_table + [no_mech_x_range]
        curve_UL.y_ranges = mech_y_range_table_UL + [no_mech_y_range_UL]
        curve_UL.labels = self.labels + [self.label_no_mech]
        curve_UL.colors = self.colors + [self.color_no_mech]
        curve_UL.markers = self.markers + [self.marker_no_mech]
        curve_UL.title = "Throughput UL"
        curve_UL.subtitle = self.subtitle
        curve_UL.xlabel = "Theoritical percentage of busy time of the medium (%)"
        curve_UL.ylabel = "Average upstream throughput of the STAs (Mbps)"
        curve_UL.x_upper_bound = upper_bound_x_range
        curve_UL.y_upper_bound = upper_bound_y_range_UL
        curve_UL.filename = os.path.join("results_figure", "parametrization", self.contention, self.mechanism, "throughput_UL")
        curve_UL.show_figure()

        # Curve for DL
        curve_DL = Curves()
        curve_DL.x_ranges = mech_x_range_table + [no_mech_x_range]
        curve_DL.y_ranges = mech_y_range_table_DL + [no_mech_y_range_DL]
        curve_DL.labels = self.labels + [self.label_no_mech]
        curve_DL.colors = self.colors + [self.color_no_mech]
        curve_DL.markers = self.markers + [self.marker_no_mech]
        curve_DL.title = "Throughput DL"
        curve_DL.subtitle = self.subtitle
        curve_DL.xlabel = "Theoritical percentage of busy time of the medium (%)"
        curve_DL.ylabel = "Average downstream throughput of the STAs (Mbps)"
        curve_DL.x_upper_bound = upper_bound_x_range
        curve_DL.y_upper_bound = upper_bound_y_range_DL
        curve_DL.filename = os.path.join("results_figure", "parametrization", self.contention, self.mechanism, "throughput_DL")
        curve_DL.show_figure()
        

    def plot_delay(self):
        mech_x_range_table = []
        mech_y_range_table_UL = []
        mech_y_range_table_DL = []
        no_mech_x_range = []
        no_mech_y_range_UL = []
        no_mech_y_range_DL = []

        for result_dict in self.result_dict_table:
            mech_x_range = []
            mech_y_range_UL = []
            mech_y_range_DL = []
            for load in result_dict:
                mech_x_range.append(result_dict[load]["Theoritical saturation"] * 100)
                mech_y_range_UL.append(result_dict[load]["UL delay avg"] * 10**3)
                mech_y_range_DL.append(result_dict[load]["DL delay avg"] * 10**3)
            mech_x_range_table.append(mech_x_range)
            mech_y_range_table_UL.append(mech_y_range_UL)
            mech_y_range_table_DL.append(mech_y_range_DL)

        # No mechanism
        for load in self.result_dict_no_mech:
            no_mech_x_range.append(self.result_dict_no_mech[load]["Theoritical saturation"] * 100)
            no_mech_y_range_UL.append(self.result_dict_no_mech[load]["UL delay avg"] * 10**3)
            no_mech_y_range_DL.append(self.result_dict_no_mech[load]["DL delay avg"] * 10**3)

        # Curve for UL
        curve_UL = Curves()
        curve_UL.x_ranges = mech_x_range_table + [no_mech_x_range]
        curve_UL.y_ranges = mech_y_range_table_UL + [no_mech_y_range_UL]
        curve_UL.labels = self.labels + [self.label_no_mech]
        curve_UL.colors = self.colors + [self.color_no_mech]
        curve_UL.markers = self.markers + [self.marker_no_mech]
        curve_UL.title = "Delay UL"
        curve_UL.subtitle = self.subtitle
        curve_UL.xlabel = "Theoritical percentage of busy time of the medium (%)"
        curve_UL.ylabel = "Average delay on upstream frames (ms)"
        curve_UL.filename = os.path.join("results_figure", "parametrization", self.contention, self.mechanism, "delay_UL")
        curve_UL.show_figure()

        # Curve for DL
        curve_DL = Curves()
        curve_DL.x_ranges = mech_x_range_table + [no_mech_x_range]
        curve_DL.y_ranges = mech_y_range_table_DL + [no_mech_y_range_DL]
        curve_DL.labels = self.labels + [self.label_no_mech]
        curve_DL.colors = self.colors + [self.color_no_mech]
        curve_DL.markers = self.markers + [self.marker_no_mech]
        curve_DL.title = "Delay DL"
        curve_DL.subtitle = self.subtitle
        curve_DL.xlabel = "Theoritical percentage of busy time of the medium (%)"
        curve_DL.ylabel = "Average delay on downstream frames (ms)"
        curve_DL.filename = os.path.join("results_figure", "parametrization", self.contention, self.mechanism, "delay_DL")
        curve_DL.show_figure()

        
    def plot_doze(self):
        mech_x_range_table = []
        mech_y_range_table = []
        no_mech_x_range = []
        no_mech_y_range = []
        upper_bound_x_range = []
        upper_bound_y_range = []

        for result_dict in self.result_dict_table:
            mech_x_range = []
            mech_y_range = []
            for load in result_dict:
                mech_x_range.append(result_dict[load]["Theoritical saturation"] * 100)
                mech_y_range.append(result_dict[load]["doze avg"] * 100)
            mech_x_range_table.append(mech_x_range)
            mech_y_range_table.append(mech_y_range)

        for load in self.result_dict_no_mech:
            # No mechanism
            no_mech_x_range.append(self.result_dict_no_mech[load]["Theoritical saturation"] * 100)
            no_mech_y_range.append(self.result_dict_no_mech[load]["doze avg"] * 100)
            # Upper bound
            th_STA_saturation = self.result_dict_no_mech[load]["Theoritical saturation"]
            th_STA_saturation /= self.result_dict_no_mech[load]["Number of STAs"]
            doze_time = 1 - th_STA_saturation
            upper_bound_x_range.append(self.result_dict_no_mech[load]["Theoritical saturation"] * 100)
            upper_bound_y_range.append(100 * doze_time)

        curve = Curves()
        curve.x_ranges = mech_x_range_table + [no_mech_x_range]
        curve.y_ranges = mech_y_range_table + [no_mech_y_range]
        curve.labels = self.labels + [self.label_no_mech]
        curve.colors = self.colors + [self.color_no_mech]
        curve.markers = self.markers + [self.marker_no_mech]
        curve.title = "Doze time of the STAs"
        curve.subtitle = self.subtitle
        curve.xlabel = "Theoritical percentage of busy time of the medium (%)"
        curve.ylabel = "Average percentage of doze time of the STAs (%)"
        curve.x_upper_bound = upper_bound_x_range
        curve.y_upper_bound = upper_bound_y_range
        curve.filename = os.path.join("results_figure", "parametrization", self.contention, self.mechanism, "doze")
        curve.show_figure()

        

if __name__ == "__main__":
    main()
