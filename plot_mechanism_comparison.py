import matplotlib.pyplot as plt
from matplotlib.patches import Polygon
import os
import json
import sys
from datetime import datetime
import math

from common_parameters import *

def main():
    result_directory_name = sys.argv[1]
    if not os.path.isdir(result_directory_name):
        print("ERROR: This program takes as an argument the directory name that contains the simulation results.")
        return -1
    contention = sys.argv[2] # in LOW, MID, HIGH
    balancing = sys.argv[3] # in EQ, UL, DL
    load = sys.argv[4] # between 005 and 100
    
    plot_curves = Plot_mechanism_comparison(result_directory_name, contention, balancing, load)
    plot_curves.plot_throughput_energy()
    # plot_curves.plot_delay_energy()

class Plot_mechanism_comparison():

    def __init__(self, result_directory_name, contention, balance, load_str):
        self.result_directory_name = result_directory_name
        self.contention = contention
        self.balance = balance
        self.load_str = load_str
        self.subtitle = ""
        self.get_subtitle()

        
        # Results
        self.no_mech_dict = dict()
        
        self.UL_slot_dict_table = []
        self.UL_slot_param_table = []
        
        self.DL_slot_dict_table = []
        self.DL_slot_param_table = []
        
        self.DL_prompt_dict_table = []
        self.DL_prompt_param_table = []
        
        self.UL_DL_slot_dict_table = []
        self.UL_DL_slot_param_table = []
        
        self.UL_slot_DL_prompt_dict_table = []
        self.UL_slot_DL_prompt_param_table = []

        self.load_results()


    def load_results(self):
        for dir_name in os.listdir(os.path.join(self.result_directory_name, self.contention)):
            result_file = open(os.path.join(self.result_directory_name, self.contention, dir_name, "results.json"))
            tot_dict = json.load(result_file)
            res_dict = tot_dict["load" + self.load_str]
            if dir_name.startswith("FFF"):
                # No mechanism
                self.no_mech_dict = res_dict
            elif dir_name.startswith("TFF"):
                # UL slot
                param_txt = dir_name.split("_")[1]
                param = int(param_txt)
                insertion_index = 0
                while insertion_index < len(self.UL_slot_param_table) and param < self.UL_slot_param_table[insertion_index]:
                    insertion_index += 1
                self.UL_slot_dict_table.insert(insertion_index, res_dict)
                self.UL_slot_param_table.insert(insertion_index, param)
            elif dir_name.startswith("FTF"):
                # DL slot
                param_txt = dir_name.split("_")[1]
                param = int(param_txt)
                insertion_index = 0
                while insertion_index < len(self.DL_slot_param_table) and param < self.DL_slot_param_table[insertion_index]:
                    insertion_index += 1
                self.DL_slot_dict_table.insert(insertion_index, res_dict)
                self.DL_slot_param_table.insert(insertion_index, param)                
            elif dir_name.startswith("FFT"):
                # DL_Prompt
                param_txt = dir_name.split("_")[1]
                param = int(param_txt)
                insertion_index = 0
                while insertion_index < len(self.DL_prompt_param_table) and param < self.DL_prompt_param_table[insertion_index]:
                    insertion_index += 1
                self.DL_prompt_dict_table.insert(insertion_index, res_dict)
                self.DL_prompt_param_table.insert(insertion_index, param)
            elif dir_name.startswith("TTF"):
                # UL slot + DL slot
                param_1_txt = dir_name.split("_")[1]
                param_2_txt = dir_name.split("_")[2]
                param = (int(param_1_txt), int(param_2_txt))
                insertion_index = 0
                while insertion_index < len(self.UL_DL_slot_param_table) \
                      and param[0] < self.UL_DL_slot_param_table[insertion_index][0] \
                      and param[1] < self.UL_DL_slot_param_table[insertion_index][1]:
                    insertion_index += 1
                self.UL_DL_slot_dict_table.insert(insertion_index, res_dict)
                self.UL_DL_slot_param_table.insert(insertion_index, param)
            elif dir_name.startswith("TFT"):
                # UL slot + DL_prompt
                param_1_txt = dir_name.split("_")[1]
                param_2_txt = dir_name.split("_")[2]
                param = (int(param_1_txt), int(param_2_txt))
                insertion_index = 0
                while insertion_index < len(self.UL_slot_DL_prompt_param_table) \
                      and param[0] < self.UL_slot_DL_prompt_param_table[insertion_index][0] \
                      and param[1] < self.UL_slot_DL_prompt_param_table[insertion_index][1]:
                    insertion_index += 1
                self.UL_slot_DL_prompt_dict_table.insert(insertion_index, res_dict)
                self.UL_slot_DL_prompt_param_table.insert(insertion_index, param)
            result_file.close()

    def get_subtitle(self):
        if self.contention == "LOW":
            self.subtitle = "2 STAs ;"
        elif self.contention == "MID":
            self.subtitle = "7 STAs ; "
        elif self.contention == "HIGH":
            self.subtitle = "20 STAs ; "
        load = int(self.load_str)
        self.subtitle += " Load: " + str(load) + "%"

        
    def plot_throughput_energy(self):
        # self.plot_throughput_energy_numbers()
        self.plot_throughput_energy_markers()


    def plot_throughput_energy_numbers(self):
        pass

    def plot_throughput_energy_markers(self):
        # x_ranges: throughputs
        # y_ranges: energy consumptions

        fig, ax = plt.subplots(1,1)

        
        # Add the demanded throughput as the further right limit.
        th_throughput = self.no_mech_dict["Theoritical tot throughput avg"] / 10**6
        plt.axvline(x=0)
        plt.axvline(x=th_throughput)
        plt.axhline(y=0)

        # No mechanism
        x_no_mech_Mbps = self.no_mech_dict["Tot throughput avg"] / 10**6
        y_no_mech = self.no_mech_dict["Consumption avg"]
        plt.plot(x_no_mech_Mbps, y_no_mech, color="black", marker="o")

        # UL slot
        x_range_UL_slot = []
        y_range_UL_slot = []
        coordinates_UL_slot = [] # The data are ordered differently, to trace the polygon (zip doesn't work for that)
        for i in range(len(self.UL_slot_dict_table)):
            throughput_Mbps = self.UL_slot_dict_table[i]["Tot throughput avg"] / 10**6
            consumption = self.UL_slot_dict_table[i]["Consumption avg"]
            x_range_UL_slot.append(throughput_Mbps)
            y_range_UL_slot.append(consumption)
            coordinates_UL_slot.append((throughput_Mbps, consumption))
            # Individually plot the markers
            marker = UL_slot_markers[str(self.UL_slot_param_table[i])]
            plt.plot(throughput_Mbps, consumption, color="blue", marker=marker, markersize=10)
        polygon_UL_slot = Polygon(coordinates_UL_slot, facecolor="blue", alpha=0.2)
        ax.add_patch(polygon_UL_slot)
        plt.plot(x_range_UL_slot, y_range_UL_slot, color="blue", marker="None", alpha=0.2)

        # DL slot
        x_range_DL_slot = []
        y_range_DL_slot = []
        coordinates_DL_slot = [] # The data are ordered differently, to trace the polygon (zip doesn't work for that)
        for i in range(len(self.DL_slot_dict_table)):
            throughput_Mbps = self.DL_slot_dict_table[i]["Tot throughput avg"] / 10**6
            consumption = self.DL_slot_dict_table[i]["Consumption avg"]
            x_range_DL_slot.append(throughput_Mbps)
            y_range_DL_slot.append(consumption)
            coordinates_DL_slot.append((throughput_Mbps, consumption))
            # Individually plot the markers with the correct color
            color = DL_slot_colors[str(self.DL_slot_param_table[i])]
            plt.plot(throughput_Mbps, consumption, color=color, marker="o")
        polygon_DL_slot = Polygon(coordinates_DL_slot, facecolor="red", alpha=0.2)
        ax.add_patch(polygon_DL_slot)
        plt.plot(x_range_DL_slot, y_range_DL_slot, color="red", marker="None" , alpha=0.2)

        # DL_Prompt
        x_range_DL_prompt = []
        y_range_DL_prompt = []
        coordinates_DL_prompt = [] # The data are ordered differently, to trace the polygon (zip doesn't work for that)
        for i in range(len(self.DL_prompt_dict_table)):
            throughput_Mbps = self.DL_prompt_dict_table[i]["Tot throughput avg"] / 10**6
            consumption = self.DL_prompt_dict_table[i]["Consumption avg"]
            x_range_DL_prompt.append(throughput_Mbps)
            y_range_DL_prompt.append(consumption)
            coordinates_DL_prompt.append((throughput_Mbps, consumption))
            # Individually plot the markers with the correct color
            color = DL_prompt_colors[str(self.DL_prompt_param_table[i])]
            plt.plot(throughput_Mbps, consumption, color=color, marker="o")
        polygon_DL_prompt = Polygon(coordinates_DL_prompt, facecolor="yellow", alpha=0.2)
        ax.add_patch(polygon_DL_prompt)
        plt.plot(x_range_DL_prompt, y_range_DL_prompt, color="yellow", marker="None" , alpha=0.2)

        # UL slot + DL slot
        x_range_UL_DL_slot = []
        y_range_UL_DL_slot = []
        coordinates_UL_DL_slot = [] # The data are ordered differently, to trace the polygon (zip doesn't work for that)
        for i in range(len(self.UL_DL_slot_dict_table)):
            throughput_Mbps = self.UL_DL_slot_dict_table[i]["Tot throughput avg"] / 10**6
            consumption = self.UL_DL_slot_dict_table[i]["Consumption avg"]
            coordinates_UL_DL_slot.append((throughput_Mbps, consumption))
            marker = UL_slot_markers[str(self.UL_DL_slot_param_table[i][0])]
            color = UL_DL_slot_colors[str(self.UL_DL_slot_param_table[i][1])]
            plt.plot(throughput_Mbps, consumption, color=color, marker=marker, markersize=10)
        convex_hull_UL_DL_slot = self.compute_convex_hull(coordinates_UL_DL_slot)
        polygon_UL_DL_slot = Polygon(convex_hull_UL_DL_slot, facecolor="purple", alpha=0.2)
        ax.add_patch(polygon_UL_DL_slot)

        # UL slot + DL_prompt
        x_range_UL_slot_DL_prompt = []
        y_range_UL_slot_DL_prompt = []
        coordinates_UL_slot_DL_prompt = []
        for i in range(len(self.UL_slot_DL_prompt_dict_table)):
            throughput_Mbps = self.UL_slot_DL_prompt_dict_table[i]["Tot throughput avg"] / 10**6
            consumption = self.UL_slot_DL_prompt_dict_table[i]["Consumption avg"]
            coordinates_UL_slot_DL_prompt.append((throughput_Mbps, consumption))
            marker = UL_slot_markers[str(self.UL_slot_DL_prompt_param_table[i][0])]
            color = UL_slot_DL_prompt_colors[str(self.UL_slot_DL_prompt_param_table[i][1])]
            plt.plot(throughput_Mbps, consumption, color=color, marker=marker, markersize=10)
        convex_hull_UL_slot_DL_prompt = self.compute_convex_hull(coordinates_UL_slot_DL_prompt)
        polygon_UL_slot_DL_prompt = Polygon(convex_hull_UL_slot_DL_prompt, facecolor="green", alpha=0.2)
        ax.add_patch(polygon_UL_slot_DL_prompt)

        
        plt.suptitle("Throughput vs. energy consumption", fontsize=17)
        plt.title(self.subtitle, fontsize=11)
        plt.xlabel("Average total throughput per STA (Mbps)", fontsize=12)
        plt.ylabel("Average energy consumption of the STA (W)", fontsize=12)
        plt.grid(linestyle=":")
        if SAVE:
            plt.savefig(os.path.join("results_figure", "mechanism_comparison", self.contention, "load" + self.load_str + ".png"))
        if SHOW:
            plt.show()
        else:
            plt.clf()

    def compute_convex_hull(self, coordinates_table):
        result_table = []
        # First point: min in x axis
        initial_point = min(coordinates_table)
        result_table.append(initial_point)
        current_point = initial_point
        current_angle = 0
        while True:
            # Next point: turn clockwise from the current angle
            next_point = None
            next_angle = 0
            for point in coordinates_table:
                angle = 0
                if current_point[0] < point[0] and current_point[1] < point[1]:
                    # Point is in the upper right corner of current point
                    angle = math.atan((point[0] - current_point[0]) / (point[1] - current_point[1]))
                elif current_point[0] < point[0] and current_point[1] > point[1]:
                    # Point is in the lower right corner of current_point
                    angle = math.pi / 2
                    angle += math.atan((current_point[1] - point[1]) / (point[0] - current_point[0]))
                elif current_point[0] > point[0] and current_point[1] < point[1]:
                    # Point is in the upper left corner of current_point
                    angle = 3*math.pi / 2
                    angle += math.atan((point[1] - current_point[1]) / (current_point[0] - point[0]))
                elif current_point[0] > point[0] and current_point[1] > point[1]:
                    # Point is in the lower left corner of current_point
                    angle = math.pi
                    angle += math.atan((current_point[0] - point[0]) / (current_point[1] - point[1]))
                elif current_point[0] == point[0] and current_point[1] < point[1]:
                    # Point is directly above current_point
                    angle = 2*math.pi
                elif current_point[0] == point[0] and current_point[1] > point[1]:
                    # Point is directly below current_point
                    angle = math.pi
                elif current_point[0] < point[0] and current_point[1] == point[1]:
                    # Point is directly on the rigth of current_point
                    angle = math.pi / 2
                elif current_point[0] > point[0] and current_point[1] == point[1]:
                    # Point is directly on the left of current_point
                    angle = 3 * math.pi / 2
                elif current_point == point:
                    angle = 100
                if next_point is None:
                    next_point = point
                    next_angle = angle
                else:
                    rel_angle = angle - current_angle
                    if rel_angle < 0:
                        rel_angle += 2*math.pi
                    rel_next_angle = next_angle - current_angle
                    if rel_next_angle < 0:
                        rel_next_angle += 2*math.pi
                    if rel_angle < rel_next_angle:
                        next_point = point
                        next_angle = angle
            result_table.append(next_point)
            current_point = next_point
            current_angle = next_angle
            if current_point == initial_point:
                break
        return result_table

        
class Plot_curves_parameters2():

    def __init__(self, result_directory_name, contention, balance, load):
        self.result_directory_name = result_directory_name
        self.contention = contention
        self.balance = balance
        self.load_txt = load
        self.subtitle = ""
        self.get_subtitle()
        
        self.dir_names = []
        self.load_dir_names()

        self.no_mech = dict()
        self.UL_slot = dict()
        self.DL_slot = dict()
        self.UL_slot_DL_slot = dict()
        self.DL_prompt = dict()
        self.UL_slot_DL_prompt = dict()

        self.load_results()

    def plot_delay_energy(self):
        self.scatter_points(dict_key_x = "Tot delay avg", dict_key_y = "Consumption avg",
                            factor_x = 10**3, factor_y = 1,
                            title = "Delay vs. energy consumption",
                            x_label = "Average delay of frames sent by STAs (UL and DL) (ms)???",
                            y_label = "Average energy consumption of STAs (W)",
                            save_name = "delay_vs_consumption")
        


    def plot_throughput_energy(self):
        self.scatter_points(dict_key_x = "Tot throughput avg", dict_key_y = "CCA avg",
                            factor_x = 10**-6, factor_y = 1,
                            title = "Throughput vs. energy consumption",
                            x_label = "Average throughput of the STAs (UL and DL) (Mbps)",
                            y_label = "Average energy consumption of STAs (W)",
                            save_name = "th_vs_consumption")

        

    def load_dir_names(self):
        self.dir_names = ["FFF", "TFF", "FTF", "TTF", "FFT", "TFT"]
        for name in self.dir_names:
            name += "cont" + self.contention
            name += "bal" + self.balance

    def load_results(self):
        for dir_name in os.listdir(os.path.join(self.result_directory_name, self.contention)):
            print("Directory name: ", dir_name)
            if dir_name.startswith("FFF"):
                # No mechanism used
                result_file = open(os.path.join(self.result_directory_name, self.contention, dir_name, "results.json"), "r")
                tmp_dict = json.load(result_file)
                result_file.close()
                if "load" + self.load_txt in tmp_dict:
                    self.no_mech = tmp_dict["load" + self.load_txt]

            elif dir_name.startswith("TFF"):
                # UL slot
                param = int(dir_name.split("_")[1])
                result_file = open(os.path.join(self.result_directory_name, self.contention, dir_name, "results.json"), "r")
                tmp_dict = json.load(result_file)
                result_file.close()
                if "load" + self.load_txt in tmp_dict:
                    self.UL_slot[param] = tmp_dict["load" + self.load_txt]

            elif dir_name.startswith("FTF"):
                # DL slot
                param = int(dir_name.split("_")[1])
                result_file = open(os.path.join(self.result_directory_name, self.contention, dir_name, "results.json"), "r")
                tmp_dict = json.load(result_file)
                result_file.close()
                if "load" + self.load_txt in tmp_dict:
                    self.DL_slot[param] = tmp_dict["load" + self.load_txt]

            elif dir_name.startswith("TTF"):
                # UL slot + DL slot
                param1 = int(dir_name.split("_")[1])
                #param2 = int(dir_name.split("_")[2])
                result_file = open(os.path.join(self.result_directory_name, self.contention, dir_name, "results.json"), "r")
                tmp_dict = json.load(result_file)
                result_file.close()
                if "load" + self.load_txt in tmp_dict:
                    #self.UL_slot_DL_slot[str(param1) + "," + str(param2)] = tmp_dict["load" + self.load_txt]
                    self.UL_slot_DL_slot[param1] = tmp_dict["load" + self.load_txt]

            elif dir_name.startswith("FFT"):
                # DL_Prompt
                param = int(dir_name.split("_")[1])
                result_file = open(os.path.join(self.result_directory_name, self.contention, dir_name, "results.json"), "r")
                tmp_dict = json.load(result_file)
                result_file.close()
                if "load" + self.load_txt in tmp_dict:
                    self.DL_prompt[param] = tmp_dict["load" + self.load_txt]

            elif dir_name.startswith("TFT"):
                # UL slot + DL_prompt
                param1 = dir_name.split("_")[1]
                param2 = dir_name.split("_")[2]
                result_file = open(os.path.join(self.result_directory_name, self.contention, dir_name, "results.json"), "r")
                tmp_dict = json.load(result_file)
                result_file.close()
                if "load" + self.load_txt in tmp_dict:
                    self.UL_slot_DL_prompt[param1 + "," + param2] = tmp_dict["load" + self.load_txt]

            tmp = dict(sorted(self.UL_slot.items()))
            self.UL_slot = tmp
            tmp = dict(sorted(self.DL_slot.items()))
            self.DL_slot = tmp
            tmp = dict(sorted(self.DL_prompt.items()))
            self.DL_prompt = tmp
            tmp = dict(sorted(self.UL_slot_DL_slot.items()))
            self.UL_slot_DL_slot = tmp

    def scatter_points(self, dict_key_x, dict_key_y, factor_x, factor_y, title, x_label, y_label, save_name):
        labels = ["No mechanism used", "UL slot", "DL slot", "DL_Prompt", "UL slot and DL slot", "UL slot and DL_prompt"]
        colors = ["Black", "#0080ff", "#ff3300", "#ffbf00", "#b30086", "#88cc00"]
        x_ranges = [[] for i in range(6)]
        y_ranges = [[] for i in range(6)]
        
        x_ranges[0].append(self.no_mech[dict_key_x] * factor_x)
        y_ranges[0].append(self.no_mech[dict_key_y] * factor_y)

        for param in self.UL_slot:
            x_ranges[1].append(self.UL_slot[param][dict_key_x] * factor_x)
            y_ranges[1].append(self.UL_slot[param][dict_key_y] * factor_y)

        for param in self.DL_slot:
            x_ranges[2].append(self.DL_slot[param][dict_key_x] * factor_x)
            y_ranges[2].append(self.DL_slot[param][dict_key_y] * factor_y)

        for param in self.DL_prompt:
            x_ranges[3].append(self.DL_prompt[param][dict_key_x] * factor_x)
            y_ranges[3].append(self.DL_prompt[param][dict_key_y] * factor_y)

        for param in self.UL_slot_DL_slot:
            x_ranges[4].append(self.UL_slot_DL_slot[param][dict_key_x] * factor_x)
            y_ranges[4].append(self.UL_slot_DL_slot[param][dict_key_y] * factor_y)

        for param in self.UL_slot_DL_prompt:
            x_ranges[5].append(self.UL_slot_DL_prompt[param][dict_key_x] * factor_x)
            y_ranges[5].append(self.UL_slot_DL_prompt[param][dict_key_y] * factor_y)

        marker_number = 0
        params = [None, self.UL_slot, self.DL_slot, self.DL_prompt, self.UL_slot_DL_slot, self.UL_slot_DL_prompt]
        result_figure = plt.figure()
        plt.text(x_ranges[0][0], y_ranges[0][0], marker_number, ha="center", va="center", color = colors[0])
        plt.plot(x_ranges[0][0], y_ranges[0][0], alpha=0)
        marker_number += 1
        for i in range(1,6):
            if i in [1, 2, 3, 4]:
                #plt.plot(x_ranges[i], y_ranges[i], color=colors[i], marker="x", linestyle="None")
                plt.plot(x_ranges[i], y_ranges[i], color=colors[i], linestyle=":")
            for j,(x,y) in enumerate(zip(x_ranges[i], y_ranges[i])):
                plt.plot(x, y, alpha=0)
                plt.text(x, y, marker_number, ha="center", va="center", color=colors[i])
                print(marker_number, ":", list(params[i])[j])
                marker_number += 1
        for i in range(6):
            plt.plot([], linestyle="None", marker="o", color=colors[i], label=labels[i]) # Empty plot to make the legend.
        plt.legend()
        plt.grid()
        plt.suptitle(title, fontsize = 17)
        plt.title(self.subtitle)
        plt.xlabel(x_label)
        plt.ylabel(y_label)
        if SAVE:
            name = save_name + datetime.now().strftime("%Y%m%d-%H:%M:%S")
            plt.savefig(os.path.join("results_figure", name + ".png"))
        plt.show()

        



if __name__ == "__main__":
    main()
