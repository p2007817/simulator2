import sys
import os

from simulation_module.simulation import *

def main():
    # 1st argument: name of the directory where the results will be written.
    # 2nd argument: level of contention (aka the number of STAs). LOW = 2 STAs, MID = 7 STAs, HIGH = 20 STAs
    # 3rd argument: balancing between UL and DL traffic. EQ, UL or DL.
    result_directory_name = sys.argv[1]
    if not os.path.isdir(result_directory_name):
        print("ERROR: this program takes as an argument the path to the directory where to put the simulation result")
        return -1
    contention = sys.argv[2]
    assert(contention == "LOW" or contention == "MID" or contention == "HIGH")
    UL_DL_balancing = sys.argv[3]
    assert(UL_DL_balancing == "UL" or UL_DL_balancing == "DL" or UL_DL_balancing == "EQ")


    DL_prompt_parameters = [1, 4, 8]
    slot_parameters = [1*10**-3, 5*10**-3, 10*10**-3, 50*10**-3, 100*10**-3]

    for load in range(5, 101, 5):
        print("Load: ", load)
        pid_no_mech = os.fork()
        if pid_no_mech == 0:
            print("\tNo mechanism (", os.getpid(), ")")
            time = experiment(result_directory_name, load, contention, UL_DL_balancing, False, False, False, -1, -1, -1)
            print("\tNo mechanism (", os.getpid(), ") --> OK in ", time)
            return 0
        pid_DL_prompt = os.fork()
        if pid_DL_prompt == 0:
            print("\tDL_Prompt (", os.getpid(), ")")
            for DL_prompt_parameter in DL_prompt_parameters:
                time = experiment(result_directory_name, load, contention, UL_DL_balancing, False, False, True, -1, -1, DL_prompt_parameter)
                print("\t\tDL_Prompt parameter: ", DL_prompt_parameter, " --> OK in ", time)
            return 0
        pid_UL_slot = os.fork()
        if pid_UL_slot == 0:
            print("\tUL slot (", os.getpid(), ")")
            for slot_parameter in slot_parameters:
                time = experiment(result_directory_name, load, contention, UL_DL_balancing, True, False, False, slot_parameter, -1, -1)
                print("\t\tUL slot parameter: ", slot_parameter, " --> OK in ", time)
            return 0
        pid_DL_slot = os.fork()
        if pid_DL_slot == 0:
            print("\tDL slot (", os.getpid(), ")")
            for slot_parameter in slot_parameters:
                time = experiment(result_directory_name, load, contention, UL_DL_balancing, False, True, False, -1, slot_parameter, -1)
                print("\t\tDL slot parameter: ", slot_parameter, " --> OK in ", time)
            return 0
        pid_UL_DL_slot = os.fork()
        if pid_UL_DL_slot == 0:
            print("\tUL slot + DL slot (", os.getpid(), ")")
            for UL_slot_parameter in slot_parameters:
                for DL_slot_parameter in slot_parameters:
                    time = experiment(result_directory_name, load, contention, UL_DL_balancing, True, True, False, UL_slot_parameter, DL_slot_parameter, -1)
                    print("\t\tUL slot parameter: ", UL_slot_parameter, ", DL slot parameter: ", DL_slot_parameter, " --> OK in ", time)                        
            return 0
        pid_UL_DL_prompt = os.fork()
        if pid_UL_DL_prompt == 0:
            print("\tUL slot + DL_prompt (", os.getpid(), ")")
            for slot_parameter in slot_parameters:
                for DL_prompt_parameter in DL_prompt_parameters:
                    time = experiment(result_directory_name, load, contention, UL_DL_balancing, True, False, True, slot_parameter, -1, DL_prompt_parameter)
                    print("\t\tUL slot parameter: ", slot_parameter, ", DL_prompt parameter: ", DL_prompt_parameter, " --> OK in ", time)
            return 0

        os.waitpid(pid_no_mech, 0)
        os.waitpid(pid_DL_prompt, 0)
        os.waitpid(pid_UL_slot, 0)
        os.waitpid(pid_DL_slot, 0)
        os.waitpid(pid_UL_DL_slot, 0)
        os.waitpid(pid_UL_DL_prompt, 0)
        print(f"{Fore.YELLOW}Finished for load ", load, f"{Style.RESET_ALL}")


        
def experiment(result_directory_name, load, contention, UL_DL_balancing,
               use_UL_slot, use_DL_slot, use_DL_prompt,
               UL_slot_parameter, DL_slot_parameter, DL_prompt_parameter):
    total_load = load * 10**6 # Out of 100 Mbps
    dir_name = ""

    # Simulation creation
    simu = Simulation()
    nb_STAs = 7
    if contention == "LOW":
        nb_STAs = 2
    elif contention == "HIGH":
        nb_STAs = 20

    individual_load = total_load / nb_STAs
    UL_load = math.floor(individual_load / 2)
    DL_load = math.floor(individual_load / 2)
    if UL_DL_balancing == "UL":
        UL_load = math.floor(9 * individual_load / 10)
        DL_load = math.floor(individual_load / 10)
    elif UL_DL_balancing == "DL":
        UL_load = math.floor(individual_load / 10)
        DL_load = math.floor(9 * individual_load / 10)


    # Run simulation
    simu.set_nb_STAs(nb_STAs)
    for i in range(1, nb_STAs+1):
        simu.set_DL_throughput(i, DL_load)
        simu.set_UL_throughput(i, UL_load)
        if use_DL_slot:
            interval = DL_slot_parameter
            duration = DL_slot_parameter / nb_STAs
            start = (i-1) * duration
            simu.toggle_DL_slot(i, start, duration, interval)
        if use_DL_prompt:
            prompt_interval = DL_prompt_parameter / simu.event_handler.contenders[i].DL_frame_generator.frame_frequency
            simu.toggle_DL_prompt(i, prompt_interval)
        if use_UL_slot:
            interval = UL_slot_parameter
            duration = UL_slot_parameter
            start = (i-1) * duration
            simu.toggle_UL_slot(i, start, duration, interval)
    
    simu.simulation_duration = 2 * 60 # 2 minute
    simu.run_simulation()


    # Write down results
    load_str = ""
    if math.floor(total_load / 10**6) < 10:
        load_str = "00" + str(math.floor(total_load / 10**6))
    elif math.floor(total_load / 10**6) < 100:
        load_str = "0" + str(math.floor(total_load / 10**6))
    else:
        load_str = str(math.floor(total_load / 10**6))        
    file_name = "load" + load_str + ".json"
    dir_name = ""
    if use_UL_slot:
        dir_name += 'T'
    else:
        dir_name += 'F'
    if use_DL_slot:
        dir_name += 'T'
    else:
        dir_name += 'F'
    if use_DL_prompt:
        dir_name += 'T'
    else:
        dir_name += 'F'
    dir_name += "cont" + contention[:3]
    dir_name += "bal" + UL_DL_balancing[:2]

    if use_UL_slot:
        dir_name += "_" + str(round(UL_slot_parameter * 10**3))
    if use_DL_slot:
        dir_name += "_" + str(round(DL_slot_parameter * 10**3))
    if use_DL_prompt:
        dir_name += "_" + str(DL_prompt_parameter)
    simu.write_down_record(os.path.join(result_directory_name, contention, dir_name), file_name)

    return simu.chrono_end - simu.chrono_start

    

if __name__=="__main__":
    main()
