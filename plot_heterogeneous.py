import sys
import json
import matplotlib.pyplot as plt
import math
from matplotlib.patches import Polygon
import os

SAVE = True
SHOW = False


def main():
    result_file_path = sys.argv[1]
    figure_dir = sys.argv[2]
    f = open(result_file_path, "r")
    full_result_dict = json.load(f)
    f.close()

    for scenario in full_result_dict:
        scenario_dict = full_result_dict[scenario]
    # scenario = "BACKeco_STAeco_40"
    # scenario_dict = full_result_dict["BACKeco_STAeco_40"]
        results_per_strategy = Results_per_strategy(scenario_dict)

        fig, ax = plt.subplots(1,1)

        # Plot curves

        plt.plot(results_per_strategy.no_strategy_results[0], results_per_strategy.no_strategy_results[1],
                 marker = "*", color = "black")
        plt.axhline(results_per_strategy.no_strategy_results[1], color="black", linewidth=1, linestyle="--")
        plt.axvline(results_per_strategy.no_strategy_results[0], color="black", linewidth=1, linestyle="--")

        marker_number = 1

        color = "#e6b800"
        for res in results_per_strategy.DL_slot_results:
            plt.text(res[0], res[1], marker_number, ha="center", va="center", color = color)
            plt.plot(res[0], res[1], linestyle="None")
            marker_number += 1
        convex_hull = compute_convex_hull(results_per_strategy.DL_slot_results)
        polygon_DL_slot = Polygon(convex_hull, facecolor=color, alpha=0.2)
        ax.add_patch(polygon_DL_slot)

        color = "magenta"
        for res in results_per_strategy.DL_prompt_results:
            plt.text(res[0], res[1], marker_number, ha="center", va="center", color = color)
            plt.plot(res[0], res[1], linestyle="None")
            marker_number += 1
        convex_hull = compute_convex_hull(results_per_strategy.DL_prompt_results)
        polygon_DL_prompt = Polygon(convex_hull, facecolor=color, alpha=0.2)
        ax.add_patch(polygon_DL_prompt)

        color = "cyan"
        for res in results_per_strategy.UL_slot_results:
            plt.text(res[0], res[1], marker_number, ha="center", va="center", color = color)
            plt.plot(res[0], res[1], linestyle="None")
            marker_number += 1
        convex_hull = compute_convex_hull(results_per_strategy.UL_slot_results)
        polygon_UL_slot = Polygon(convex_hull, facecolor=color, alpha=0.2)
        ax.add_patch(polygon_UL_slot)

        color = "red"
        for res in results_per_strategy.UL_prompt_results:
            plt.text(res[0], res[1], marker_number, ha="center", va="center", color = color)
            plt.plot(res[0], res[1], linestyle="None")
            marker_number += 1
        convex_hull = compute_convex_hull(results_per_strategy.UL_prompt_results)
        polygon_UL_prompt = Polygon(convex_hull, facecolor=color, alpha=0.2)
        ax.add_patch(polygon_UL_prompt)

        color = "green"
        for res in results_per_strategy.DL_slot_UL_slot_results:
            plt.text(res[0], res[1], marker_number, ha="center", va="center", color = color)
            plt.plot(res[0], res[1], linestyle="None")
            marker_number += 1
        convex_hull = compute_convex_hull(results_per_strategy.DL_slot_UL_slot_results)
        polygon_DL_slot_UL_slot = Polygon(convex_hull, facecolor=color, alpha=0.2)
        ax.add_patch(polygon_DL_slot_UL_slot)

        color = "#e67300"
        for res in results_per_strategy.DL_slot_UL_prompt_results:
            plt.text(res[0], res[1], marker_number, ha="center", va="center", color = color)
            plt.plot(res[0], res[1], linestyle="None")
            marker_number += 1
        convex_hull = compute_convex_hull(results_per_strategy.DL_slot_UL_prompt_results)
        polygon_DL_slot_UL_prompt = Polygon(convex_hull, facecolor=color, alpha=0.2)
        ax.add_patch(polygon_DL_slot_UL_prompt)

        color = "purple"
        for res in results_per_strategy.DL_prompt_UL_slot_results:
            plt.text(res[0], res[1], marker_number, ha="center", va="center", color = color)
            plt.plot(res[0], res[1], linestyle="None")
            marker_number += 1
        convex_hull = compute_convex_hull(results_per_strategy.DL_prompt_UL_slot_results)
        polygon_DL_prompt_UL_slot = Polygon(convex_hull, facecolor=color, alpha=0.2)
        ax.add_patch(polygon_DL_prompt_UL_slot)

        plt.grid(linestyle=":")
        plt.xlabel("Total throughput of the considered STA (Mbps)", fontsize = 14)
        plt.ylabel("Energy consumption of the considered STA (W)", fontsize=14)
        if SAVE:
            plt.savefig(os.path.join(figure_dir, scenario + ".png"))
        if SHOW:
            plt.show()
        else:
            plt.clf()


def get_title(scenario_label):
    BACK_label = scenario_label.split("_")[0]
    STA_label = scenario_label.split("_")[1]
    saturation_label = scenario_label.split("_")[2]

    if BACK_label[4:] == "eco":
        pass
    elif BACK_label[4:] == "perf":
        pass
    else:
        assert(0)

    if STA_label[3:] == "eco":
        pass
    elif STA_label[3:] == "perf":
        pass
    else:
        assert(0)

    title = "jesaispas"
    subtitle = saturation_label + "% saturation in background"

    return (title, subtitle)



class Results_per_strategy():

    def __init__(self, scenario_dict):
        self.no_strategy_results = None
        self.DL_slot_results = []
        self.DL_slot_parameters = []
        self.DL_prompt_results = []
        self.DL_prompt_parameters = []
        self.UL_slot_results = []
        self.UL_slot_parameters = []
        self.UL_prompt_results = []
        self.UL_prompt_parameters = []
        self.DL_slot_UL_slot_results = []
        self.DL_slot_UL_slot_parameters = []
        self.DL_slot_UL_prompt_results = []
        self.DL_slot_UL_prompt_parameters = []
        self.DL_prompt_UL_slot_results = []
        self.DL_prompt_UL_slot_parameters = []

        self.get_values(scenario_dict)

    def get_values(self, scenario_dict):
        for strategy in scenario_dict:
            result_throughput = scenario_dict[strategy]["Tot throughput STA"]["1"] / (10**6)
            result_consumption = scenario_dict[strategy]["Consumption STA"]["1"]
            result = (result_throughput, result_consumption)
            parameter = None
            if strategy == "no_strategy":
                self.no_strategy_results = result
            elif strategy.startswith("DL_slot_UL_slot"):
                self.DL_slot_UL_slot_results.append(result)
                parameter = (strategy.split("_")[-2], strategy.split("_")[-1])
                self.DL_slot_parameters.append(parameter)
            elif strategy.startswith("DL_slot_UL_prompt"):
                self.DL_slot_UL_prompt_results.append(result)
                parameter = (strategy.split("_")[-2], strategy.split("_")[-1])
                self.DL_slot_UL_prompt_parameters.append(parameter)
            elif strategy.startswith("DL_prompt_UL_slot"):
                self.DL_prompt_UL_slot_results.append(result)
                parameter = (strategy.split("_")[-2], strategy.split("_")[-1])
                self.DL_prompt_UL_slot_parameters.append(parameter)
            elif strategy.startswith("DL_slot"):
                self.DL_slot_results.append(result)
                parameter = strategy.split("_")[-1]
                self.DL_slot_parameters.append(parameter)
            elif strategy.startswith("DL_prompt"):
                self.DL_prompt_results.append(result)
                parameter = strategy.split("_")[-1]
                self.DL_prompt_parameters.append(parameter)
            elif strategy.startswith("UL_slot"):
                self.UL_slot_results.append(result)
                parameter = strategy.split("_")[-1]
                self.UL_slot_parameters.append(parameter)
            elif strategy.startswith("UL_prompt"):
                self.UL_prompt_results.append(result)
                parameter = strategy.split("_")[-1]
                self.UL_prompt_parameters.append(parameter)
            else:
                print("Unexpected file name: ", strategy)
                
def compute_convex_hull(coordinates_table):
    result_table = []
    # First point: min in x axis
    initial_point = min(coordinates_table)
    result_table.append(initial_point)
    current_point = initial_point
    current_angle = 0
    while True:
        # Next point: turn clockwise from the current angle
        next_point = None
        next_angle = 0
        for point in coordinates_table:
            angle = 0
            if current_point[0] < point[0] and current_point[1] < point[1]:
                # Point is in the upper right corner of current point
                angle = math.atan((point[0] - current_point[0]) / (point[1] - current_point[1]))
            elif current_point[0] < point[0] and current_point[1] > point[1]:
                # Point is in the lower right corner of current_point
                angle = math.pi / 2
                angle += math.atan((current_point[1] - point[1]) / (point[0] - current_point[0]))
            elif current_point[0] > point[0] and current_point[1] < point[1]:
                # Point is in the upper left corner of current_point
                angle = 3*math.pi / 2
                angle += math.atan((point[1] - current_point[1]) / (current_point[0] - point[0]))
            elif current_point[0] > point[0] and current_point[1] > point[1]:
                # Point is in the lower left corner of current_point
                angle = math.pi
                angle += math.atan((current_point[0] - point[0]) / (current_point[1] - point[1]))
            elif current_point[0] == point[0] and current_point[1] < point[1]:
                # Point is directly above current_point
                angle = 2*math.pi
            elif current_point[0] == point[0] and current_point[1] > point[1]:
                # Point is directly below current_point
                angle = math.pi
            elif current_point[0] < point[0] and current_point[1] == point[1]:
                # Point is directly on the rigth of current_point
                angle = math.pi / 2
            elif current_point[0] > point[0] and current_point[1] == point[1]:
                # Point is directly on the left of current_point
                angle = 3 * math.pi / 2
            elif current_point == point:
                angle = 100
            if next_point is None:
                next_point = point
                next_angle = angle
            else:
                rel_angle = angle - current_angle
                if rel_angle < 0:
                    rel_angle += 2*math.pi
                rel_next_angle = next_angle - current_angle
                if rel_next_angle < 0:
                    rel_next_angle += 2*math.pi
                if rel_angle < rel_next_angle:
                    next_point = point
                    next_angle = angle
        result_table.append(next_point)
        current_point = next_point
        current_angle = next_angle
        if current_point == initial_point:
            break
    return result_table


if __name__=="__main__":
    main()
