from simulation_module.simulation import *
from computings_module.results import *

def main_homogeneous():
    simulation = Simulation()
    simulation.simulation_duration = 2 #s
    nb_STAs = 2
    UL_load = 9.216 * 10**6
    DL_load = 9.216 * 10**6
    link_capacity = 144 * 10**6
    simulation.set_nb_STAs(nb_STAs)
    for i in range(1, nb_STAs+1):
        simulation.set_DL_throughput(i, DL_load, is_interval_random=False)
        simulation.set_UL_throughput(i, UL_load, is_interval_random=False)
        simulation.set_link_capacity(i, link_capacity)

    simulation.run_simulation()

    report = simulation.get_report()
    results = Results(None, None, report)
    print()
    print("=== Results ===")
    print("Tot UL throughput: ", results.UL_throughput_avg * nb_STAs)
    print("Tot DL throughput: ", results.DL_throughput_avg * nb_STAs)
    print("Tot throughput: ", (results.UL_throughput_avg + results.DL_throughput_avg) * nb_STAs)
    print(results.DL_sent_frames_STA)
    print(results.UL_sent_frames_STA)
    UL_sent_frames = sum(results.UL_sent_frames_STA)
    DL_sent_frames = sum(results.DL_sent_frames_STA)
    print("UL sent frames: ", UL_sent_frames)
    print("DL sent frames: ", DL_sent_frames)

    
def main_heterogeneous():
    simulation = Simulation()
    simulation.simulation_duration = 2 #s
    nb_STAs = 2
    STA1_load = 144 * 10**6
    STA2_load = 9.216 * 10**6
    simulation.set_nb_STAs(nb_STAs)
    simulation.set_DL_throughput(1, STA1_load, is_interval_random=False)
    simulation.set_UL_throughput(1, STA1_load, is_interval_random=False)
    for i in range(2, nb_STAs+1):
        simulation.set_DL_throughput(i, STA2_load, is_interval_random=False)
        simulation.set_UL_throughput(i, STA2_load, is_interval_random=False)
    
    simulation.run_simulation()

    STA_dict = simulation.get_STAs_dictionary()
    event_dict = simulation.simulator_core.record.get_dictionary()
    tot_dict = {
        "STAs": STA_dict,
        "Events": event_dict
    }
    results = Results(None, None, tot_dict)
    print()
    print("=== Results ===")
    print("STA 1")
    print("\tUL frames:", results.UL_sent_frames_STA[0])
    print("\tDL frames:", results.DL_sent_frames_STA[0])
    print("STA 2")
    print("\tUL frames:", results.UL_sent_frames_STA[1])
    print("\tDL frames:", results.DL_sent_frames_STA[1])



if __name__=="__main__":
    main_homogeneous()
